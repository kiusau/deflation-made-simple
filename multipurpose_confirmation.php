<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Spirit of 2021 - Mount Cambitas eMail Verification</title>
	<meta name="generator" content="BBEdit 11.6" />
	<style>
		header {
			font-weight: bold;
		}
		
		header .subtitle {
			margin-top: -0.9em;
		}
				
		span.name {
			font-family:'Bradley Hand',cursive;
			font-size:1.6em;
		}

		div#wrapper {
			width: 500px;
			margin: auto;
		}
		
		#wrapper::before {
            content: "";
            position: absolute;
            top: 0px;
            right: 0px;
            bottom: 0px;
            left: 0px;
  
            /* Specify the background image to be used */
            background-image: url('_images/email_ornament_original.png');
            background-size: cover;
            opacity: 0.9;
        }
        #content {
			margin: 0;
			position: absolute;
			top: 50%;
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);
        }	
				
		#message_body {
			font-size: 1.2em;
			vertical-align:bottom;
			max-width: 600px;
		}
		
		footer {
			display: flex;
			justify-content: space-between;
			flex-wrap: wrap;
		}
		
		footer .right_box {
			text-align: right;
		}

		a.link_style:link {color:#000; text-decoration: none;}
		a.link_style:visited {color:#000;}
/* 
		a.link_style:link {color:#c0c0c0; font-weight: bold; text-decoration: none;} 
		a.link_style:visited {color:#fadb9d; font-weight: bold;}
		a.link_style:hover {color:#fadb9d; font-weight: bold;}
		a.link_style:active {color:#5a4149; font-weight: bold;}
 */

	</style>
</head>
<body>
	<div id='wrapper'>
		<div id='content'>
			<header>
			<h1>Spirit of 2021</h1>
			<p class='subtitle'>Mount Cambitas eMail Verification</p>
			</header>
			<section style='padding-left:1em;'>
				<div id='message_body'>
					<?php
						if (!empty($_GET['name']) && !empty($_GET['msg'])) {
							echo $_GET['msg'];
						}
					?>
				</div><!-- div#message_body -->
				<div id='salutation'>
					<p style='font-size: 1.2em;'>Wishing you my very best!</p>
					<p><span class='name'>Roddy A. Stegemann</span><br />In liberty, or not at all!<br />Your host at the home of the Spirit of 2021.</p>
				</div><!-- end div#salutation -->
			</section>
			<footer>
				<div>
					<span style="font-style: italic;">Webmaster and Design: </span>Hashimori Iwato<br />
					<a class='link_style' href="https://www.hashimori.com" title="Hashimori Iwato Homepage" target="_blank">http://www.hashimori.com</a>
				</div>
				<div class='right_box'>
					<?php echo date("l") . ", " . date("j F Y");?> <br />Seattle, Washington 98104 USA
				</div><!-- end div.footer_item_right -->
			</footer>
		</div><!-- end div#content -->
	</div><!-- end div#wrapper -->
</body>
</html>
