/**
** Page Title:  page_entry.js
** Locatioin: file:///Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/js/page_entry.js
** Author:  Roddy A. Stegemann
** Creation Date:  Friday, 25 March 2022
** Description:  Process the form in page_entry.html and interface with the page_entry.php processing file to insure that data is properly sent and retrieved in a readable format.
**/

function createEntry(event) {
	event.preventDefault();
	const pageData = new FormData(event.target);
	const pageJSON = Object.fromEntries(pageData.entries());
	pageJSON.day = pageData.getAll('pageno');
	pageJSON.time = pageData.getAll('chapter');
	pageJSON.hour = pageData.getAll('page_format');
	pageJSON.min = pageData.getAll('destination');
	pageJSON.min = pageData.getAll('update');
	pageJSON.min = pageData.getAll('page_content');
	var pageStr = JSON.stringify(pageJSON,null);
	var pageObj = JSON.parse(pageStr);
	$.post('./_utilities/php/page_entry.php', {pageObj: pageObj}, function(data_entry) {
		$("#status_update").append("Congratulations! Your data was posted.");
// 		$.each(JSON.parse(data_entry), function(key, value) {
// 			if (key == "page") {
// 				$("#status_update").html("Current Page: " + value);
// 			}
// 		});
	});
}
