/*  Javascript for Spirit of 2021 Splash Page
**  Respective URL:  spiritof2021/index.html
**  Date of Creation:  2018/10/21
**  Author:  Roddy A. Stegemann
*/

/**** Test for DOM Storage ****/

$( document ).ready(function() {
/***************************************************
Create a schedule, send it to rm.php for processing,
receive the users GUID with a schedule confirmation,
and display the confirmation.
****************************************************/
	var omittedItem = [];
	var newSchedule = [];
	var feedset = {};
	var sortDex = [];
	var schedule = [];
	var result = [];
	var jsonSet = [];
	var index = 0;
	var count =0;
	var itemDex =0;
	var cronjobs = [];
	let i = 0;
	function handleFormSubmit(event) {
		event.preventDefault();
		const data = new FormData(event.target);
		const formJSON = Object.fromEntries(data.entries());
		formJSON.day = data.getAll('day');
		formJSON.time = data.get('time');
		formJSON.hour = data.get('hour');
		formJSON.min = data.get('min');
		const schdlStr = JSON.stringify(formJSON, null);
		const jsonObj = JSON.parse(schdlStr);
		var displayItem = '';
		$.each(jsonObj, function(index, element) {
			if(index  == 'day') {
				let result = [];
				$.each(element, function(key, value) {
					if (value == 'every') {
						result.push('Every day');
					}
					if (value == 'week') {
						result.push('Weekdays');
					}
					switch (value) {
						case 'mon' :
							result[0] = 'Monday';
							break;
						case 'tue' :
							result.push('Tuesday');
							break;
						case 'wed' :
							result.push('Wednesday');
							break;
						case 'thu' :
							result.push('Thursday');
							break;
						case 'fri' :
							result.push('Friday');
							break;
						case 'sat' :
							result.push('Saturday');
							break;
						case 'sun' :
							result.push('Sunday');
							break;
					}
				});
				function setDay(result) {
					let myDay= '';
					var i = 0;
					if (result.length > 2){
						do {
							myDay += result[i] + ', ';
							i++;
						} while (i < result.length - 1);
						myDay += ' and ' + result[result.length-1];
						return myDay;
					}	
					if (result.length == 1) {
						return result;
					}
					if (result.length = 2) {
						i = 0;
						do {
							myDay += result[i] + ' ';
							i++;
						} while (i < result.length - 1);
						myDay += ' and ' + result[result.length-1];
						return myDay;
					}
				}
				displayItem = "<button class='delete'>X</button>" + setDay(result);
			}
			if(index == 'time') {
				setAmPm = ' ' + element;
			}
			if(index == 'hour') {
				setTime = ' at ' + element + ':';
			}
			if(index == 'min') {
				setTime += element + setAmPm;
			}
		});

		function setItem(displayItem, setTime, jsonObj) {
			let count = i++;
			var schdlItem = "<div class='item' data-index='" + count + "'>" + displayItem + setTime + "</div>";
			$('#schedule').append(schdlItem);		
			schedule.push(schdlItem);
			jsonSet.push(jsonObj);
			return result = [schedule, jsonSet, count];
		}
		result = setItem(displayItem, setTime, jsonObj);		
		$("div[data-index]").one('click', result, modSelect);
		/***************************************************
				Set first schedule 
		****************************************************/
		$('#confirm').click(function(event) {
			event.stopImmediatePropagation();
			var userTZ = Intl.DateTimeFormat().resolvedOptions().timeZone;
			$.post('./_utilities/php/rm.php', {jsonSet: jsonSet, schedule: schedule, timezone: userTZ}, function(data) {
					$('#schedule').html("Congratulations!<br>Your schedule has been entered as follows:<br><br>");
// 					$('#schedule').html(data);
					$.each(JSON.parse(data), function(key, value) {
						if (key == 0) {
							cronjobs = value;
						}
						if (key == 1){
							$('#guid').append(value[0]);
							
							for (let i = 1; i < value.length; i++) {
								$('#schedule').append(value[i] + "<br />");
							}
						}
						if (key == "timezone") {
							$('#schedule').append(value + " (timezone: approximate location)<br />");
							$('#timezone').append(value);
						} 
					});
					$('#schedule').append("<br />You may change your schedule as many times as you like before pressing the confirmation button.  In order to do so, however, you must reenter your schedule in its entirety! Please forgive me for the inconvenience.<br /><span style='font-family: Bradley Hand, cursive;';'><br />The Webmaster</span> <br /><br />");
				}).done(function() {
					$('div2schedule').html('Your schedule has been successfully uploaded and a copy will be sent to the email with which you subscribe. Please see below!');
				}).fail(function() {
					alert('There was an error.  Please try again, or use the contact information provided on the previous page and notify the webmaster!');
			});
		});
		$('#reschedule').click(function(event) {
			event.stopImmediatePropagation();
			var userTZ = Intl.DateTimeFormat().resolvedOptions().timeZone;
			$.post('./_utilities/php/schdmod.php', {jsonSet: jsonSet, schedule: schedule, timezone: userTZ}, function(data) {
					$('#schedule').html("Congratulations!<br>Your schedule has been entered as follows:<br><br>");
// 					$('#schedule').html(data);
					$.each(JSON.parse(data), function(key, value) {
						if (key == 0) {
							cronjobs = value; 	
						}
						if (key == 1){
							$('#guid').append(value[0]);
							
							for (let i = 1; i < value.length; i++) {
								$('#schedule').append(value[i] + "<br />");
							}
						}
						if (key == "timezone") {
							$('#schedule').append(value + " (timezone: approximate location)<br />");
							$('#timezone').append(value);
						} 
					});
					$('#schedule').append("<br />You may change your schedule as many times as you like before pressing the confirmation button.  In order to do so, however, you must reenter your schedule in its entirety! Please forgive me for the inconvenience.<br /><span style='font-family: Bradley Hand, cursive;';'><br />The Webmaster</span> <br /><br />");
				}).done(function() {
					$('div2schedule').html('Your schedule has been successfully uploaded and a copy will be sent to the email with which you subscribe. Please see below!');
				}).fail(function() {
					alert('There was an error.  Please try again, or use the contact information provided on the previous page and notify the webmaster!');
			});
		});
	}
	function modSelect(event) {
		event.stopImmediatePropagation();
		schedule = event.data[0];
		jsonSet = event.data[1];
		itemDex = $(this).index();
		omittedItem = schedule.splice(itemDex,1);
		jsonSet.splice(itemDex,1);
		$(this).remove();
// 		return schedule;
	}
	const form = document.querySelector('#createEntry');
	form.addEventListener('submit', handleFormSubmit);
	/***************************************************
	Send GUID back to rm.php for further processing,
	entry of the user's schedule into the Crontab, and
	access to the MySQL database.
	****************************************************/
	const formTwo = document.querySelector('#payment');
	formTwo.addEventListener('submit', handlerFormTwoSubmit);
	function handlerFormTwoSubmit(event) {
		event.preventDefault();
		var username = '';
		var email = '';
		var guid = $('#guid').html();
		var paypass = '';
		if ($('#timezone').html()) {
			var timezone = $('#timezone').html();
		} else {
			var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
		}
		const data = new FormData(event.target);
		data.append("guid",guid);
		const formValues = Object.fromEntries(data.entries());
		$.each(formValues, function(key, value) {
			if (key == "username") {
				username = value;
			}
			if (key == "email") {
				email = value;
			}
			if (key == "paypass") {
				paypass = value;
			}
			if (key == "payment") {
				if (value == 0) {
					$.post('./_utilities/php/rm.php', {payment: value, guid: 0, username: username, email: email, timezone: timezone}, function(data) {
						$("#confirmation").html(data);
					});
// 					$.post('./_utilities/php/rm.php', {payment: value, guid: guid}, function(data) {
// 						$("#payment_option").load("../../gift_production.html");
// 					});
				} else if (value == 1) {
					$.post('./_utilities/php/rm.php', {payment: value, guid: guid, username: username, email: email, cronjobs: cronjobs, timezone: timezone}, function(data) {
						$("#confirmation").html(data);
					});
				} else if (value == 2) {
					$.post('./_utilities/php/rm.php', {payment: value, guid: guid, username: username, email: email, cronjobs: cronjobs, timezone: timezone}, function(data) {
						$("#confirmation").html(data);
					});
				} else if (value == 3) {
					$.post('./_utilities/php/rm.php', {payment: value, guid: guid, username: username, email: email, cronjobs: cronjobs, timezone: timezone, paypass: paypass}, function(data) {
						$("#confirmation").html(data);
					});
				}
			}
		});
// 		const formTwoJSON = Object.fromEntries(data.entries());
	}
	/***************************************************
		Reschedule, Pause, and New Passphrase 
	****************************************************/
	const formThree = document.querySelector('#recreate_entry');
	formThree.addEventListener('submit', handlerFormThreeSubmit);
	function handlerFormThreeSubmit(event) {
		event.preventDefault();
		var guid = $('#guid').html();
		if ($('#timezone').html()) {
			var timezone = $('#timezone').html();
		} else {
			var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
		}
		var subscriber = '';
		var subscriber_email = '';
		var passphrase = '';
		var newphrase = new Boolean(false);
		var freechap = new Boolean(false);
		var pause_restart = new Boolean(false);
		var reschedule = new Boolean(false);
		var unsubscribe = new Boolean(false);
		var pageno = 0;
		var newSchedule = cronjobs;
		const data = new FormData(event.target);
		const reformJSON = Object.fromEntries(data.entries());
		$.each(reformJSON, function(key, value) {
			if (key == "subscriber") {
				subscriber = value;
			}
			if (key == "subscriber_email") {
				subscriber_email = value;
			}
			if (key == "passphrase") {
				passphrase = value;
			}
			if (key == "newphrase") {
				newphrase = value;
			}
			if (key == "freechap") {
				freechap = value;
			}
			if (key == "pause_restart") {
				pause_restart = value;
			}
			if (key == "reschedule") {
				reschedule = value;
			}
			if (key == "page_number") {
				pageno = value;
			}
			if (key == "unsubscribe") {
				unsubscribe = value;
			}
		});
		if (newSchedule.length == 0) {
			$.post('./_utilities/php/schdmod.php', {username: subscriber, email: subscriber_email, passphrase: passphrase, newphrase: newphrase, freechap: freechap, pause_restart: pause_restart, unsubscribe: unsubscribe, pageno: pageno}, function(data) {
				$("#schedule_revision").html(data);
			});

		} else {
			$.post('./_utilities/php/schdmod.php', {username: subscriber, email: subscriber_email, guid: guid, passphrase: passphrase, reschedule: reschedule, pageno: pageno, cronjobs: newSchedule, timezone: timezone}, function(data) {
				$("#schedule_revision").html(data);
			});
		}
	}	
});
