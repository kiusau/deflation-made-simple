<?php
/**
** Page Title:  page_entry.php
** Locatioin: file:///Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/page_entry.php
** Author:  Roddy A. Stegemann
** Creation Date:  Friday, 25 March 2022
** Description:  Provide the MySQLi and PHP inteface between the page_entry.js processing page and the spirit.repository table.
**/
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);

	include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
	$papa_connect = new PapaConnect();
	$mysqli_obj = $papa_connect->get_mysqli_obj();
	$tbl_name = 'spirit_db.repository';
// include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/_utilities/php/classes/class.iwatoconnect.php");
// 	$iwato_connect = new IwatoConnect();
// 	$mysqli_obj = $iwato_connect->get_mysqli_obj();
// 	$tbl_name = 'spirit.repository';
	$html = "";
	$text = "";
	$recent = [];
	if (isset($_POST['status'])) {
		$status = filter_var($_POST['status'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH);
		if ($status == "ready") {
			$sql = "SELECT page, chapter, html, text FROM $tbl_name ORDER BY page DESC LIMIT 1";
			$result_obj = $mysqli_obj->query($sql);
			while ($row = $result_obj->fetch_assoc()) {
				foreach ($row as $field => $value) {
					if ($field == "page") {
						$recent["page"] = $value;
					}
					if ($field == "chapter") {
						$recent["chapter"] = $value;
					}
					if ($field == "html") {
						if (!empty($value)) {
							$recent["html"] = "full";
						} else {
							$recent["html"] = "empty";
						}
					}
					if ($field == "text") {
						if (!empty($value)) {
							$recent["text"] = "full";
						} else {
							$recent["text"] = "empty";
						}
					}
				}
			}
		}
		echo json_encode($recent);
	}
	if(isset($_POST['pageObj'])) {
		$page_obj = $_POST['pageObj'];
		$pageno = filter_var($page_obj['pageno'], FILTER_VALIDATE_INT);
		$chapter = filter_var($page_obj['chapter'], FILTER_VALIDATE_INT);
		$format = filter_var($page_obj['page_format'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$destination = filter_var($page_obj['destination'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$page_content = filter_var($page_obj['page_content'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$update = filter_var($page_obj['update'], FILTER_VALIDATE_INT);
// 		if ($destination == "local") {
		if ($destination == "remote") {
			$sql = "SELECT obs FROM $tbl_name WHERE page=? AND chapter=?";
			$mysqli_stmt = $mysqli_obj->stmt_init();
			$mysqli_stmt->prepare($sql);
			$mysqli_stmt->bind_param("ii", $pageno, $chapter);
			$mysqli_stmt->execute();
			$mysqli_result = $mysqli_stmt->get_result();
			$match = mysqli_num_rows($mysqli_result);
			if($match > 0){				
				while ($row = $mysqli_result->fetch_assoc()) {
					foreach ($row as $field => $value) {
						if ($field == 'obs') {
							$obs = $value;
						}
						if ($field == 'html') {
							$html = $value;
						}
						if ($field == 'text') {
							$text = $value;
						}
					}
				}
				if ($format == "html") {
					$html = $page_content;
					$sql = "UPDATE $tbl_name SET html = ? WHERE obs = ? AND page = ?";
					$mysqli_stmt = $mysqli_obj->stmt_init();
					$mysqli_stmt->prepare($sql);
					$mysqli_stmt->bind_param("sii", $html, $obs, $pageno);
					$mysqli_stmt->execute();
				} else if ($format == "text") {
					$text = $page_content;
					$sql = "UPDATE $tbl_name  SET text = ? WHERE obs = ? AND page = ?";
					$mysqli_stmt = $mysqli_obj->stmt_init();
					$mysqli_stmt->prepare($sql);
					$mysqli_stmt->bind_param("sii", $text, $obs, $pageno);
					$mysqli_stmt->execute();
				}
				$mysqli_result->free_result;				
			} else {
				if ($format == "html") {
					$html = $page_content;
					$text = NULL;
					$query = "INSERT INTO $tbl_name (page, chapter, html, text) VALUES (?,?,?,?)";
					$stmt = $mysqli_obj->prepare($query);
					$stmt->bind_param("iiss", $pageno, $chapter, $html, $text);
					$stmt->execute();
				} else if ($format == "text") {
					$text = $page_content;
					$html = NULL;
					$query = "INSERT INTO $tbl_name (page, chapter, html, text) VALUES (?,?,?,?)";
					$stmt = $mysqli_obj->prepare($query);
					$stmt->bind_param("iiss", $pageno, $chapter, $html, $text);
					$stmt->execute();
				}
			}
			if ($update == 1) {
				if ($format == "html") {
					$html = $page_content;
					$sql = "UPDATE $tbl_name  SET html = ? WHERE obs = ? AND page = ?";
					$mysqli_stmt = $mysqli_obj->stmt_init();
					$mysqli_stmt->prepare($sql);
					$mysqli_stmt->bind_param("sii", $html, $obs, $pageno);
					$mysqli_stmt->execute();
				} else if ($format == "text") {
					$text = $page_content;
					$sql = "UPDATE $tbl_name  SET text = ? WHERE obs = ? AND page = ?";
					$mysqli_stmt = $mysqli_obj->stmt_init();
					$mysqli_stmt->prepare($sql);
					$mysqli_stmt->bind_param("sii", $text, $obs, $pageno);
					$mysqli_stmt->execute();
				}
			}
		}
	}
	$mysqli_obj->close();
?>