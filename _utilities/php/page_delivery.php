<?php
/*
** Filename: page_delivery.php
** Location: /Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php
** Corresponding Files:  Called from the CRON engine.
** Creation Date: Tuesday, 12 April 2022
** Author: Hashimori Iwato
** Brief Description:  This page delivers the reader's book page at regular intervals determined
** by the calling cronjob identified by the reader's guid and email.
** Linkage: Recieves data from the Crontab.
**** Access the spirit_db database via class.papaconnect.php.
**** Access the PHPMailer namespace via autoload.php
****   
*/
	/****************************************************************************
		Set up the TiBeN Namespace for the PHP Include
	****************************************************************************/
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
	/****************************************************************************
		Create namespace for PHPMailer and TiBeN CrontabManager
	****************************************************************************/
	require_once("/home/thege0/vendor/autoload.php");
// 	require_once("/Users/iwato/vendor/autoload.php");
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;
	use TiBeN\CrontabManager\CrontabJob;
	use TiBeN\CrontabManager\CrontabRepository;
	use TiBeN\CrontabManager\CrontabAdapter;
	/****************************************************************************
		Identify reader and process deliver of book page.
	****************************************************************************/
	if (isset($argv)) {
		/************************************************************************************
			Variable initialization.
		************************************************************************************/
// 		$matched = [];
		$guid = '';
		$email = '';
		$pageno = 0;
		$paymetn = 0;
		$username = "";
		$tempage_html = "";
		$chapter = 0;
		$timezone = '';
		/*************************************************************************************
			Set page limit.
		*************************************************************************************/
		$page_limit = 15;
		$bookend = 280;
		/************************************************************************************
			Ready the arguments received from the CRON call for matching with subscriber.
		************************************************************************************/
		foreach($argv as $arg) {
			parse_str($arg, $item);
			foreach($item as $key => $value) {
				if ($key == 'guid') {
					$guid = $value;
// 					$guid = strstr($value, "-", true);
				}
				if ($key == 'email') {
					$email = $value;
				}
			}
		}
		/************************************************************************************
			Read the contents of the $argv array to $match_result as a string.
		************************************************************************************/
// 		ob_start();
// 		print_r($argv);
// 		$matched_result = ob_get_clean();
		/********************************************************************************
			Query subscriber data table for payment status and pageno 
		********************************************************************************/
		include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
		$papa_connect = new PapaConnect();
		$mysqli_obj = $papa_connect->get_mysqli_obj();
// 		include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 		$iwato_connect = new IwatoConnect();
// 		$mysqli_obj = $iwato_connect->get_mysqli_obj();
		$tbl_name = "spirit_db.subscribers";
// 		$tbl_name = "spirit.subscribers";
		$sql_select = "SELECT payment, pageno FROM $tbl_name WHERE guid=?";
		$mysqli_stmt = $mysqli_obj->stmt_init();
		$mysqli_stmt->prepare($sql_select);
		$mysqli_stmt->bind_param("s", $guid);
		$mysqli_stmt->execute();
		$meta = $mysqli_stmt->result_metadata();
		while ($field = $meta->fetch_field()) {
			$params[] = &$row[$field->name];
		}
		call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
		while ($mysqli_stmt->fetch()) {
			foreach($row as $key => $val) {
				$c[$key] = $val;
			}
			$prelim_result[] = $c;
		}
		foreach ($prelim_result as $arr) {
			foreach ($arr as $name => $value){
				$page_results[$name] = $value;
			}
		}
		$mysqli_stmt->free_result();
		$payment = $page_results['payment'];
		$pageno = $page_results['pageno'];
		/************************************************************
			Fetch the appropirate banner image.
		*************************************************************/
		$image_link = "<img src='cid:aveverum_banner' max-width='600' width:100% height='145' style='margin:0; padding:0; border:none; display:block;' border='0' alt='The Ave Verum Banner' />";
		$tempage_html = '';
// 		$tempage_text = '';
		/*************************************************************************************
			Distribute scheduled user's first page.
		*************************************************************************************/
		if ($pageno <= 0) {
			$pageno = 1;
			$chapter = 1;
			/************************************************************
			Obtain subscriber and page data for scheduled patron.
			*************************************************************/
			include_once("/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php");
// 			include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php");
			$page_data = new PageData($mysqli_obj, $guid, $pageno);
// 			var_dump($page_data);
			$delivery_data = $page_data->get_subscriber_data($guid);
			$username = $delivery_data['username'];
			$email = $delivery_data['email'];
// 			$chapter = $delivery_data['chapter'];
			$timezone = $delivery_data['timezone'];
			$message_html = $page_data->get_page_data($pageno, "html");
			$message_html = $message_html['html'];
// 			$message_text = $page_data->get_page_data(0, "text");		
// 			$message_text = $message_text['text'];
			$html = $delivery_data['html'];
// 			$text = $subscriber_data['text'];
			/************************************************************
			Create the $date_time sring.
			*************************************************************/
			$timestamp = time();
			$date_time = new DateTime("now", new DateTimeZone($timezone));
			$date_time->setTimestamp($timestamp);
			$date = $date_time->format('D, d M Y, H:i:s');
			/************************************************************
			Create the $tags Array and fill the text and html templates.
			*************************************************************/
			require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 			require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 			if (!empty($text)) {
			if (isset($text)) {
	// 			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 				$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 				$tempage_text = new SimplePage($template);
// 				$tags_text = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'msg_paytype' => $text, 'message' => $message_text);
// 				$tempage_text->replace_tags($tags_text);
			}
			if (!empty($html)) {
				$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_book.html';
// 				$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_book.html';
				$tempage_html = new SimplePage($template);
				$tags_html = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'msg_paytype' => $html, 'message' => $message_html, 'image' => $image_link, 'chapter' => $chapter, 'page' => 1);
				$tempage_html->replace_tags($tags_html);
			}
			/*************************************************************************************
				Update user's page status.
			*************************************************************************************/
			$sql_update = "UPDATE $tbl_name SET pageno=? WHERE guid=? AND email=?";
			$mysqli_stmt->prepare($sql_update);
			$mysqli_stmt->bind_param("iss", $pageno, $guid, $email);
			$mysqli_stmt->execute();
			$mysqli_stmt->free_result();						
		/*************************************************************************************
			Send warning message to subscriber and disable CRONjob.
		*************************************************************************************/
		} else if (($pageno >= $page_limit) && ($payment == 1)) {
			$pageno = $page_limit;	
			$chapter = 1;	
			include_once("/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php");
// 			include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php");
			$page_data = new PageData($mysqli_obj, $guid, $pageno);
// 			var_dump($page_data);
			$delivery_data = $page_data->get_subscriber_data($guid);
			$username = $delivery_data['username'];
			$email = $delivery_data['email'];
// 			$chapter = $delivery_data['chapter'];
			$timezone = $delivery_data['timezone'];
// 			$message_html = $page_data->get_page_data($pageno, "html");
// 			$message_html = $message_html['html'];
// 			$message_text = $page_data->get_page_data(0, "text");		
// 			$message_text = $message_text['text'];
			$html = $delivery_data['html'];
// 			$text = $subscriber_data['text'];
			/************************************************************
			Create the $dte_time sring.
			*************************************************************/
			$timestamp = time();
			$date_time = new DateTime("now", new DateTimeZone($timezone));
			$date_time->setTimestamp($timestamp);
			$date = $date_time->format('D, d M Y, H:i:s');
			/************************************************************
			Create the $tags Array and fill the text and html templates.
			*************************************************************/
			require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 			require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 			if (!empty($text)) {
			if (isset($text)) {
	// 			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 				$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 				$tempage_text = new SimplePage($template);
// 				$tags_text = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'msg_paytype' => $text, 'message' => $message_text);
// 				$tempage_text->replace_tags($tags_text);
			}
			if (!empty($html)) {
				$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_warning.html';
// 				$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_warning.html';
				$tempage_html = new SimplePage($template);
				$tags_html = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'image' => $image_link);
				$tempage_html->replace_tags($tags_html);
			}
			/************************************************************
				Turn off the user's CRONjob
			*************************************************************/
			$crontabRepository = new CrontabRepository(new CrontabAdapter());		
			$cronjobs = $crontabRepository->findJobByRegex("/$guid/");
			foreach($cronjobs as $cronjob) {
				foreach ($cronjob as $key => $value) {
					if ($key == 'enabled') {
						$cronjob->setEnabled(false);
					}
				}
			}
			$crontabRepository->persist();
		/*************************************************************************************
			Deliver current page and advance subscriber's pageno by one.
		*************************************************************************************/
		} else if ((($pageno >= 1) && ($pageno <= $bookend) && ($payment == 2)) || (($payment == 1) && !($pageno >= $page_limit))) {
			$newpage = $pageno + 1;
			/************************************************************
			Obtain subscriber and page data for scheduled patron.
			*************************************************************/
			include_once("/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php");
// 			include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php");
			$page_data = new PageData($mysqli_obj, $guid, $pageno);
// 			var_dump($page_data);
			$delivery_data = $page_data->get_subscriber_data($guid);
			$username = $delivery_data['username'];
			$email = $delivery_data['email'];
			$chapter = $delivery_data['chapter'];
			$timezone = $delivery_data['timezone'];
			$message_html = $page_data->get_page_data($pageno, "html");
			$message_html = $message_html['html'];
// 			$message_text = $page_data->get_page_data(0, "text");		
// 			$message_text = $message_text['text'];
			$html = $delivery_data['html'];
// 			$text = $subscriber_data['text'];
			/************************************************************
			Create the $dte_time sring.
			*************************************************************/
			$timestamp = time();
			$date_time = new DateTime("now", new DateTimeZone($timezone));
			$date_time->setTimestamp($timestamp);
			$date = $date_time->format('D, d M Y, H:i:s');
			/************************************************************
			Create the $tags Array and fill the text and html templates.
			*************************************************************/
			require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 			require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 			if (!empty($text)) {
			if (isset($text)) {
	// 			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 				$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 				$tempage_text = new SimplePage($template);
// 				$tags_text = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'msg_paytype' => $text, 'message' => $message_text);
// 				$tempage_text->replace_tags($tags_text);
			}
			if (!empty($html)) {
				$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_book.html';
// 				$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_book.html';
				$tempage_html = new SimplePage($template);
				$tags_html = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'msg_paytype' => $html, 'message' => $message_html, 'image' => $image_link, 'chapter' => $chapter, 'page' => $pageno);
				$tempage_html->replace_tags($tags_html);
			}
			/*************************************************************************************
				Update user's page status.
			*************************************************************************************/
			$sql_update = "UPDATE $tbl_name SET pageno=? WHERE guid=? AND email=?";
			$mysqli_stmt->prepare($sql_update);
			$mysqli_stmt->bind_param("iss", $newpage, $guid, $email);
			$mysqli_stmt->execute();
			$mysqli_stmt->free_result();						
		}
		/************************************************************
		Prepare the email for mailing and send.
		*************************************************************/
		$mail = new PHPMailer(true);
		/************************************************************/
		try {
	// 		$mail->SMTPDebug = SMTP::DEBUG_SERVER;
			$mail->SMTPDebug = 0;
			$mail->isSMTP();
			$mail->SMTPAuth   = true;
	// 		$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
// 			$mail->Host = "0.0.0.0";
// 			$mail->Port       = 1025; 
// 			$mail->Username   = 'iwato@jupiter.local';         
// 			$mail->Password   = 'nudge.Liberty+';                   
			$mail->Host = "nwurq132.hostpapavps.net";
			$mail->Port = 587;
			$mail->Username = "admin@spiritof2021.online";
			$mail->Password = "FCw2UxG7jmeX7L8";
			/************************************************************/
			$mail->setFrom('admin@spiritof2021.online', 'Spirit of 2021');
			$mail->addAddress($email, $username);
			$mail->addReplyTo('admin@spiritof2021.online', 'Spirit of 2021');
			/************************************************************/
			$mail->CharSet = 'UTF-8';
	// 		$mail->ContentType = CONTENT_TYPE_MULTIPART_MIXED;
	// 		$mail->Encoding = ENCODING_BINARY;
			$mail->Priority = 1;
			$mail->addEmbeddedImage('/home/thege0/public_html/spiritof2021.online/cambitas/_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
			/************************************************************/
			$mail->isHTML(true);
			$mail->Subject = "Mount Cambitas Chapter $chapter, Page $pageno";
			$mail->Body = htmlspecialchars_decode($tempage_html->output());
	// 		$mail->msgHTML = $tempage_html->output();
			$mail->altBody = "Hi, $username! Welcome to Mount Cambitas - The Story of Real Money.  If you have created a schedule for receipt of the book in page increments sent to your email box, please permit HTML formatting in your device reader or wait until the book can be obtained in a bound or PDF format -- both of which will become available when the online-email version of the book is complete.  For the moment, you may use your previously mailed passphrase to download the first chapter in PDF format or to change your schedule, if you created one.  If you have any questions, please contact the author at +1 (206) 291-8468 or kiusau@me.com.  Thank you for making yourself open to a comprehensive understanding of real money, and its vital importance to the future of our once great nation.  In liberty, or not at all, Roddy A. Stegemann";
			/************************************************************/
			$mail->send();
	// 		echo 'Message has been sent';
		} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error (1): " . $e->errorMessage();
		} catch (\Exception $e) {
			echo "Message could not be sent. Mailer Error (2): " . $e->getMessage();
		}	
	}
?>
