<?php
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
/****************************************************************************
Create response to AJAX request for User GUID and schedule confirmation.
****************************************************************************/
	require_once("/home/thege0/vendor/autoload.php");
// 	require_once("/Users/iwato/vendor/autoload.php");
	use Ramsey\Uuid\Uuid;
	use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
	use TiBeN\CrontabManager\CrontabJob;
	use TiBeN\CrontabManager\CrontabRepository;
	use TiBeN\CrontabManager\CrontabAdapter;
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	if(isset($_POST['jsonSet'])) {
		if (is_array($_POST['jsonSet'])){
			$jS_arr = $_POST['jsonSet'];
			$userTZ = filter_var($_POST['timezone'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
			$uuid = Uuid::uuid4();
			$uuid->toString();
			$schd_arr[0]=$uuid;
	// 		$crontabJob = new CrontabJob();
	// 		$crontabJob->setComments($uuid);
			$crontabJob;
			$crontabJobs = [];
			$infoPacket = [];
			$days =[];
			$cronDays = [];
			$dayStr = '';
			$cronStr = '';
			$time = '';
			$cronTime = 0;
			$hour = '';
			$min = '';
	// 		$result =[];
			function setDay($dayMultiple) {
				$dayStr = '';
				$cronStr = '';
				$result = [];
				$resultStrs = [];
				foreach ($dayMultiple as $dayValue) {
					$dayValue = filter_var($dayValue, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
					switch ($dayValue) {
						case 'every':
						$day = 'Everyday';
						$cronDay = "SUN-SAT";
						break;
						case 'week':
						$day = 'Weekdays';
						$cronDay = "MON-FRI";
						break;
						case 'mon':
						$day = 'Monday';
						$cronDay = 1;
						break;
						case 'tue':
						$day = 'Tuesday';
						$cronDay = 2;
						break;
						case 'wed':
						$day = 'Wednesday';
						$cronDay = 3;
						break;
						case 'thu':
						$day = 'Thursday';
						$cronDay = 4;
						break;
						case 'fri':
						$day = 'Friday';
						$cronDay = 5;
						break;
						case 'sat':
						$day = 'Saturday';
						$cronDay = 6;
						break;
						case 'sun':
						$day = 'Sunday';
						$cronDay = 0;
						break;
						default:
							echo "Please select a day!";
					}
					$days[] = $day;
					$cronDays[] = $cronDay;
				}
				if (count($days) == 1) {
					$dayStr = $days[0];
					$cronStr = $cronDays[0];
				} else if (count($days) == 2) {
					$dayStr = $days[0] . " and " . $days[1];
					$cronStr = $cronDays[0] . "," . $cronDays[1];
				} else {
					for($i=0; $i < (count($days)-1); $i++) {
						$dayStr .= $days[$i] . ", ";
						$cronStr .= $cronDays[$i] . ",";
					}
					$dayStr .= " and " . $days[count($days)-1];
					$cronStr .= $cronDays[count($days)-1];
				}
				$resultStrs = [$dayStr, $cronStr];
				return $resultStrs;
			}
			/**********************************************************************************
				Create an Array for schedule entries with multiple days for the same time
			**********************************************************************************/
			function setDayRun($value3) {
				$dayResult = [];
				return $dayResult = setDay($value3);
			}
			/**********************************************************************************
				Assemble multiple schedule entries for the same user.
			**********************************************************************************/
			foreach ($jS_arr as $key1 => $value1) {
				$crontabJob = new CrontabJob();
				$crontabJob->setEnabled(true);
				$crontabJob->setComments($uuid);
				foreach($value1 as $value2 => $value3) {
					if (is_array($value3)) {
						$result = setDayRun($value3);
						$dayStr = $result[0];
						$cronStr = $result[1];
						$crontabJob->setDayOfWeek($cronStr);
					}
					if ($value2 == 'time') {
						$time = filter_var($value3, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
						if ($time == 'AM') {
							$cronTime = 0;
						}
						if ($time == 'PM') {
							$cronTime = 12;
						}
					}
					if ($value2 == 'hour') {
						$hour = filter_var($value3, FILTER_VALIDATE_INT);
						$cronHour = $value3 + $cronTime;
						$crontabJob->setHours($cronHour);
					}
					if ($value2 == 'min') {
						$min = filter_var($value3, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
						$crontabJob->setMinutes($min);
					}
				}
				$schd_arr[] = $dayStr . ' at ' . $hour . ':' . $min . ' ' . $time;
				$crontabJobs[] = $crontabJob;
				unset($crontabJob);
			}
			/********************************************************************
				Create a JSON object to repond to AJAX call from scheduling.js
			*********************************************************************/
			$info_pack = array($crontabJobs, $schd_arr, "timezone" => $userTZ);
	// 		echo json_encode($crontabJobs);
	// 		echo json_encode($schd_arr);
			echo json_encode($info_pack);
		}
	}
	/**********************************************************************************
		Process the Account Info for the Subscriber Based on His Payment Selection
	**********************************************************************************/
	if (isset($_POST['payment'])) {
		/****************************************************************************
			Filter the subscriber's contact and other info sent by the AJAX routine
		****************************************************************************/
		if (isset($_POST['paypass'])) {
			$passphrase = filter_var($_POST['paypass'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH);
		}
		$payment = filter_var($_POST['payment'], FILTER_VALIDATE_INT);
// 		$pageno = filter_var($_POST['pageno'], FILTER_VALIDATE_INT);
		$guid = filter_var($_POST['guid'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH);
		$username = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$email = filter_var($_POST['email'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_SANITIZE_EMAIL);
		$minutes = 0;
		$hour = 0;
		$day_of_week = '';
		$page_limit = 15;
		$msg = '';
		$timezone = filter_var($_POST['timezone'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$image_link = "<img src='cid:aveverum_banner' max-width='600' width:100% height='145' style='margin:0; padding:0; border:none; display:block;' border='0' alt='The Ave Verum Banner' />";
		/*********************************************************************************
			Filter the contents of the prepared cronjob returned with the payment info
		*********************************************************************************/
		if (isset($_POST['cronjobs'])) {
			$cronjobs = $_POST['cronjobs'];
			function filterCronjobs($cronjobs) {
				$filters = array(
					'enabled' => FILTER_VALIDATE_BOOLEAN,
					'minutes' => FILTER_VALIDATE_INT,
					'hours' => FILTER_VALIDATE_INT,
					'dayOfMonth' => FILTER_VALIDATE_INT,
					'months' => FILTER_VALIDATE_INT,
					'dayOfWeek' => "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH",
					'taskCommandLine' => "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH",
					'comments' => "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH",
					'shortCut' =>  "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH"
				);
				foreach($cronjobs as $key => $value) {
					$filtered_cronjobs[] = filter_var_array($value, $filters);
				}
				return $cronjobs;
			}
			$cronjobs = filterCronjobs($cronjobs);
		}
		/****************************************************************************************
			Fill the subscribers' table with the subscriber's account info and time schedule 
			where appropriate and generate and send an account verification email.
		****************************************************************************************/
		if ($payment === 0) {
			if (!empty($username) && !empty($email)) {
				/****************************************************************************
					Connect to database.
				****************************************************************************/
				include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
				$papa_connect = new PapaConnect();
				$mysqli_obj = $papa_connect->get_mysqli_obj();
// 				include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 				$iwato_connect = new IwatoConnect();
// 				$mysqli_obj = $iwato_connect->get_mysqli_obj();
				/********************************************************************************
					Create guid. Set hash. Set Cronjob variable to zero. Initialize variables.
				********************************************************************************/
				$uuid = Uuid::uuid4();
				$guid = $uuid->toString();
				$hash = 0;
// 				$guid = 0;
				$payment = 0;
				$pageno = 0;
				$minutes = 0;
				$hour = 0;
				$day_of_week = '';
				/****************************************************************************
					Set table and insert new subscriber data.
				****************************************************************************/
				$tbl_name = 'spirit_db.subscribers';
// 				$tbl_name = 'spirit.subscribers';
				$query = "INSERT INTO $tbl_name (guid, username, email, passhash, payment, pageno, minutes, hour, day_of_week, timezone) VALUES (?,?,?,?,?,?,?,?,?,?)";
				$stmt = $mysqli_obj->prepare($query);
				$stmt->bind_param("ssssiiiiss", $guid, $username, $email, $hash, $payment, $pageno, $minutes, $hour, $day_of_week, $timezone);
				$stmt->execute();
				/************************************************
					Initiate PHPMailer instance
				************************************************/
				$mail = new PHPMailer(true);
				$mail->CharSet = 'UTF-8';
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				$mail->Debugoutput = 'html';
				/****************************************************************************
					Connect to spiritof2021.online mailserver.
				****************************************************************************/
				$mail->Host = "nwurq132.hostpapavps.net";
				$mail->Port = 587;
				$mail->Username = "admin@spiritof2021.online";
				$mail->Password = "FCw2UxG7jmeX7L8";
// 				$mail->Host = "0.0.0.0";
// 				$mail->Port = 1025;
// 				$mail->Username = "admin@spiritof2021.online";
// 				$mail->Password = "nudge.Liberty+";
				/****************************************************************************
					Prepare confirmation email
				****************************************************************************/
				$mail->SMTPAuth = true;
				$mail->setFrom('admin@spiritof2021.online', 'Spirit of 2021');
				$mail->addAddress($email, $username);
				$mail->Subject = 'Account Verification';
				$mail->Priority = 1;
				$mail->addEmbeddedImage('/home/thege0/public_html/spiritof2021.online/cambitas/_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
// 				$mail->addEmbeddedImage('../../_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
				/****************************************************************************
					Prepare message.
				****************************************************************************/
				$html_message = file_get_contents('/home/thege0/public_html/spiritof2021.online/cambitas/confirmation_mail.html');
// 				$html_message = file_get_contents('../../confirmation_mail.html');
				$html_message = str_replace('%image%', $image_link, $html_message);
				$html_message = str_replace('%timezone%', $timezone, $html_message);
				$html_message = str_replace('%name%', $username, $html_message);
				$html_message = str_replace('%email%', $email, $html_message);
				$html_message = str_replace('%payment%', 0, $html_message);
				$html_message = str_replace('%pageno%', 0, $html_message);
				$html_message = str_replace('%guid%', $guid, $html_message);
// 				$mail->msgHTML($html_message);
				$mail->Body = $html_message;
				$alt_message = "Congratulations,  $username! You have successfully opened an account with the Spirit of 2021. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and send you to a welcome page. \n\r  https://cambitas.spiritof2021.online/_utilities/php/email_verify.php?payment=$payment&guid=$guid \n\r If later you would like to close your account you will be provided with the opportunity.";
// 				$alt_message = "Congratulations,  $username! You have successfully opened an account with the Spirit of 2021. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and send you to a welcome page. \n\r  https://cambitas.spiritof2021.local/_utilities/php/email_verify.php?payment=$payment&guid=$guid \n\r If later you would like to close your account you will be provided with the opportunity.";
				$mail->AltBody = $alt_message;
				/****************************************************************************
					Send email or report failure.
				****************************************************************************/
				if (!$mail->send()) {
					$msg = $mail->ErrorInfo;
				} else {
					echo "<p>Hi, $username! You have been sent a confirmation email.</p><p>Unless you have received email from the Spirit of 2021 before, please check your junk box, if the email does not appear in your inbox within the next few minutes.</p><p>The purpose of the email is to authenticate you as the author of the just created account.  Click where indicated., and your action will be acknowledged.  Whereupon you will be sent a welcome email with the password to your new account.</p>";
				}
			}
		} else if ($payment === 1) {
			if (!empty($username) && !empty($email) && !empty($guid)) {
				/****************************************************************************
					Connect to database.
				****************************************************************************/
				include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
				$papa_connect = new PapaConnect();
				$mysqli_obj = $papa_connect->get_mysqli_obj();
// 				include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 				$iwato_connect = new IwatoConnect();
// 				$mysqli_obj = $iwato_connect->get_mysqli_obj();
				/****************************************************************************
					Set $hash, $payment, and $pageno to zero. Set $tbl_name. Insert Cronjobs.
				****************************************************************************/
				$hash = 0;
				$payment = 0;
				$pageno = 0;
				$tbl_name = 'spirit_db.subscribers';
// 				$tbl_name = 'spirit.subscribers';
				foreach ($cronjobs as $key) {
					foreach ($key as $name => $value) {
						if ($name == 'minutes') {
							$minutes = $value;
						}
						if ($name == 'hours') {
							$hour = $value;
						}
						if ($name == 'dayOfWeek') {
							$day_of_week = $value;
						}
					}					
					/****************************************************************************
						Insert the individual cronjobs into the database.
					****************************************************************************/
					$query = "INSERT INTO $tbl_name (guid, username, email, passhash, payment, pageno, minutes, hour, day_of_week, timezone) VALUES (?,?,?,?,?,?,?,?,?,?)";
					$stmt = $mysqli_obj->prepare($query);
					$stmt->bind_param("ssssiiiiss", $guid, $username, $email, $hash, $payment, $pageno, $minutes, $hour, $day_of_week, $timezone);
					$stmt->execute();
				}
				/****************************************************************************
					Enter the resulting cronjobs into the crontab.
				****************************************************************************/
				include('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
// 				include('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
				/****************************************************************************
					Initiate PHPMailer instance
				****************************************************************************/
				$mail = new PHPMailer(true);
				$mail->CharSet = 'UTF-8';
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				$mail->Debugoutput = 'html';
				/****************************************************************************
					Connect to spiritof2021.online mailserver.
				****************************************************************************/
				$mail->Host = "nwurq132.hostpapavps.net";
				$mail->Port = 587;
				$mail->Username = "admin@spiritof2021.online";
				$mail->Password = "FCw2UxG7jmeX7L8";
// 				$mail->Host = "0.0.0.0";
// 				$mail->Port = 1025;
// 				$mail->Username = "admin@spiritof2021.online";
// 				$mail->Password = "nudge.Liberty+";
				/****************************************************************************
					Prepare email
				****************************************************************************/
				$mail->SMTPAuth = true;
				$mail->setFrom('admin@spiritof2021.online', 'Spirit of 2021');
				$mail->addAddress($email, $username);
				$mail->Subject = 'Account Verification';
				$mail->Priority = 1;
				$mail->addEmbeddedImage('/home/thege0/public_html/spiritof2021.online/cambitas/_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
// 				$mail->addEmbeddedImage('../../_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
				/****************************************************************************
					Prepare message.
				****************************************************************************/
				$html_message = file_get_contents('/home/thege0/public_html/spiritof2021.online/cambitas/confirmation_mail.html');
// 				$html_message = file_get_contents('../../confirmation_mail.html');
				$html_message = str_replace('%image%', $image_link, $html_message);
				$html_message = str_replace('%timezone%', $timezone, $html_message);
				$html_message = str_replace('%name%', $username, $html_message);
				$html_message = str_replace('%email%', $email, $html_message);
				$html_message = str_replace('%payment%', 1, $html_message);
				$html_message = str_replace('%pageno%', 1, $html_message);
				$html_message = str_replace('%guid%', $guid, $html_message);
// 				$mail->msgHTML($html_message);
				$mail->Body = $html_message;
				$alt_message = "Congratulations,  $username! You have successfully opened an account with the Spirit of 2021. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and send you to a welcome page. \n\r  https://cambitas.spiritof2021.online/_utilities/php/email_verify.php?payment=$payment&guid=$guid \n\r If later you would like to close your account you will be provided with the opportunity.";
// 				$alt_message = "Congratulations,  $username! You have successfully opened an account with the Spirit of 2021. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and send you to a welcome page. \n\r  https://cambitas.spiritof2021.local/_utilities/php/email_verify.php?payment=$payment&guid=$guid \n\r If later you would like to close your account you will be provided with the opportunity.";
				$mail->AltBody = $alt_message;
				/****************************************************************************
					Send email or report failure.
				****************************************************************************/
				if (!$mail->send()) {
					$msg = $mail->ErrorInfo;
				} else {
					echo "<p>Hi, $username! You have been sent a confirmation email.</p><p>Unless you have received email from the Spirit of 2021 before, please check your junk box, if the email does not appear in your inbox within the next few minutes.</p><p>The purpose of the email is to authenticate you as the author of the just created account.  Click where indicated., and your action will be acknowledged.  Whereupon you will be sent a welcome mail with your new password.</p>";
				}
			/****************************************************************************
				Request schedule, if none confirmed.
			****************************************************************************/
			} else {
				echo "Please create and confirm a schedule!";
			}
		} else if ($payment === 2) {
			if (!empty($username) && !empty($email) && !empty($guid)) {
				/****************************************************************************
					Connect to database.
				****************************************************************************/
				include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
				$papa_connect = new PapaConnect();
				$mysqli_obj = $papa_connect->get_mysqli_obj();
// 				include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 				$iwato_connect = new IwatoConnect();
// 				$mysqli_obj = $iwato_connect->get_mysqli_obj();
				/****************************************************************************
					Set $hash, $payment, and $pageno to zero. Set $tbl_name. Insert Cronjobs.
				****************************************************************************/
				$hash = 0;
				$payment = 0;
				$pageno = 0;
				$tbl_name = 'spirit_db.subscribers';
// 				$tbl_name = 'spirit.subscribers';
				/****************************************************************************
					Assign the Subscriber's Schedule to Database and create New Cronjob.
				****************************************************************************/
				foreach ($cronjobs as $key) {
					foreach ($key as $name => $value) {
						if ($name == 'minutes') {
							$minutes = $value;
						}
						if ($name == 'hours') {
							$hour = $value;
						}
						if ($name == 'dayOfWeek') {
							$day_of_week = $value;
						}
					}					
					/****************************************************************************
						Insert the individual cronjobs into the database.
					****************************************************************************/
					$query = "INSERT INTO $tbl_name (guid, username, email, passhash, payment, pageno, minutes, hour, day_of_week, timezone) VALUES (?,?,?,?,?,?,?,?,?,?)";
					$stmt = $mysqli_obj->prepare($query);
					$stmt->bind_param("ssssiiiiss", $guid, $username, $email, $hash, $payment, $pageno, $minutes, $hour, $day_of_week, $timezone);
					$stmt->execute();
				}
				/****************************************************************************
					Enter the resulting cronjobs into the crontab.
				****************************************************************************/
				include('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
// 				include('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
				/****************************************************************************
					Initiate PHPMailer instance
				****************************************************************************/
				$mail = new PHPMailer(true);
				$mail->CharSet = 'UTF-8';
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				$mail->Debugoutput = 'html';
				/****************************************************************************
					Connect to spiritof2021.online mailserver.
				****************************************************************************/
				$mail->Host = "nwurq132.hostpapavps.net";
				$mail->Port = 587;
				$mail->Username = "admin@spiritof2021.online";
				$mail->Password = "FCw2UxG7jmeX7L8";
// 				$mail->Host = "0.0.0.0";
// 				$mail->Port = 1025;
// 				$mail->Username = "admin@spiritof2021.online";
// 				$mail->Password = "nudge.Liberty+";
				/****************************************************************************
					Prepare confirmation email
				****************************************************************************/
				$mail->SMTPAuth = true;
				$mail->setFrom('admin@spiritof2021.online', 'Spirit of 2021');
				$mail->addAddress($email, $username);
				$mail->Subject = 'Account Verification';
				$mail->Priority = 1;
				$mail->addEmbeddedImage('/home/thege0/public_html/spiritof2021.online/cambitas/_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
// 				$mail->addEmbeddedImage('../../_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
				/****************************************************************************
					Prepare message.
				****************************************************************************/
				$html_message = file_get_contents('/home/thege0/public_html/spiritof2021.online/cambitas/confirmation_mail.html');
// 				$html_message = file_get_contents('../../confirmation_mail.html');
				$html_message = str_replace('%image%', $image_link, $html_message);
				$html_message = str_replace('%timezone%', $timezone, $html_message);
				$html_message = str_replace('%name%', $username, $html_message);
				$html_message = str_replace('%email%', $email, $html_message);
				$html_message = str_replace('%payment%', 2, $html_message);
				$html_message = str_replace('%pageno%', 1, $html_message);
				$html_message = str_replace('%guid%', $guid, $html_message);
// 				$mail->msgHTML($html_message);
				$mail->Body = $html_message;
				$alt_message = "Congratulations,  $username! You have successfully opened an account with the Spirit of 2021. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and send you to a welcome page. \n\r  https://cambitas.spiritof2021.online/_utilities/php/email_verify.php?payment=$payment&guid=$guid \n\r If later you would like to close your account you will be provided with the opportunity.";
// 				$alt_message = "Congratulations,  $username! You have successfully opened an account with the Spirit of 2021. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and send you to a welcome page. \n\r  https://cambitas.spiritof2021.local/_utilities/php/email_verify.php?payment=$payment&guid=$guid \n\r If later you would like to close your account you will be provided with the opportunity.";
				$mail->AltBody = $alt_message;
				/****************************************************************************
					Send email or report failure.
				****************************************************************************/
				if (!$mail->send()) {
					$msg = $mail->ErrorInfo;
				} else {
					echo "<p>Hi, $username! You have been sent a confirmation email.</p><p>Unless you have received email from the Spirit of 2021 before, please check your junk box, if the email does not appear in your inbox within the next few minutes.</p><p>The purpose of the email is to authenticate you as the author of the just created account.  Click where indicated., and your action will be acknowledged.  Whereupon you will be sent a welcome mail with the password to your new account.</p>";
				}
			/****************************************************************************
				Request schedule, if none confirmed.
			****************************************************************************/
			} else if (!$guid) {
				echo "Please create and confirm a schedule!";
			}
		} else if (($payment === 3) && !empty($passphrase)) {
			if (!empty($username) && !empty($email)) {
				/****************************************************************************
					Connect to database.
				****************************************************************************/
				include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
				$papa_connect = new PapaConnect();
				$mysqli_obj = $papa_connect->get_mysqli_obj();
// 				include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 				$iwato_connect = new IwatoConnect();
// 				$mysqli_obj = $iwato_connect->get_mysqli_obj();
				/*****************************************************************************************
					Obtain the subscriber's previous subscription data based on his username and email
				*****************************************************************************************/
				$tbl_name = 'spirit_db.subscribers';
// 				$tbl_name = 'spirit.subscribers';
				$sql_select = "SELECT passhash, payment, pageno, guid FROM $tbl_name WHERE username=? AND email=?";
				$mysqli_stmt = $mysqli_obj->stmt_init();
				$mysqli_stmt->prepare($sql_select);
				$mysqli_stmt->bind_param("ss", $username, $email);
				if ($mysqli_stmt->execute()) {
					$meta = $mysqli_stmt->result_metadata();
					while ($field = $meta->fetch_field()) {
						$params[] = &$row[$field->name];
					}
					call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
					while ($mysqli_stmt->fetch()) {
						foreach($row as $key => $val) {
							$c[$key] = $val;
						}
						$prelim_result[] = $c;
					}
					$mysqli_stmt->free_result();
					if ($prelim_result) {
						$passhash = '';
						$current_page = 0;
						$old_guid = '';
						$payment = 0;
						foreach ($prelim_result as $arr) {
							foreach ($arr as $name => $value){
								if ($name == "passhash") {
									$passhash = $value;
								}
								if ($name == "payment") {
									$payment = $value;
								}
								if ($name == "pageno") {
									$current_page = $value;
								}
								if ($name == "guid") {
									$old_guid = $value;
								}
							}
						}				
						if (password_verify($passphrase, $passhash)) {
							/****************************************************************************
								Set trial user accounts who have been receiving pages to paid.
							****************************************************************************/
							if (empty($guid) && ($payment == 1) && ($current_page > 0)) {
								$pageno = $current_page + 1;
								$sql = "UPDATE $tbl_name SET payment='2', pageno=? WHERE guid=?";
								$mysqli_stmt->prepare($sql);
								$mysqli_stmt->bind_param("ss", $pageno, $old_guid);
								$mysqli_stmt->execute();
								$mysqli_stmt->free_result();
								echo "<p>Congratulations, <span class='name'> $username </span>!<br />You are now a paid subscriber.</p><p>You need take no further action.  Your next page of <em>Mount Cambitas:  The Story of Real Money</em> will be sent with your next scheduled delivery.  Your restart page will be Page $pageno.</p>";
							/****************************************************************************
								Set trial user accounts who have received no pages to paid.
							****************************************************************************/
							} else if (empty($guid) && ($payment == 1) && ($current_page == -2)) {
								$pageno = 1;
								$sql = "UPDATE $tbl_name SET payment='2', pageno=? WHERE guid=?";
								$mysqli_stmt->prepare($sql);
								$mysqli_stmt->bind_param("ss", $pageno, $old_guid);
								$mysqli_stmt->execute();
								$mysqli_stmt->free_result();
								echo "<p>Congratulations, <span class='name'> $username </span>!<br />You are now a paid subscriber.</p><p>You need take no further action.  The next page of <em>Mount Cambitas:  The Story of Real Money</em> will be sent with your first scheduled delivery.</p>";
							/****************************************************************************
								Set to paid the accounts of non-trial users with accounts.
							****************************************************************************/
							} else if ($guid && ($payment == 1) && ($current_page == -3)) {
								/****************************************************************************
									Prepare the selected time schedule for entry into database and crontab
								****************************************************************************/
								$payment = 2;
								$pageno = $page_limit + 1;
								foreach ($cronjobs as $key) {
									foreach ($key as $name => $value) {
										if ($name == 'minutes') {
											$minutes = $value;
										}
										if ($name == 'hours') {
											$hour = $value;
										}
										if ($name == 'dayOfWeek') {
											$day_of_week = $value;
										}
									}					
									/****************************************************************************
										Insert the individual cronjobs into the database.
									****************************************************************************/
									$query = "INSERT INTO $tbl_name (guid, username, email, passhash, payment, pageno, minutes, hour, day_of_week, timezone) VALUES (?,?,?,?,?,?,?,?,?,?)";
									$mysqli_stmt->prepare($query);
									$mysqli_stmt->bind_param("ssssiiiiss", $guid, $username, $email, $passhash, $payment, $pageno, $minutes, $hour, $day_of_week, $timezone);
									$mysqli_stmt->execute();
									$mysqli_stmt->free_result();
								}
								/****************************************************************************
									Enter the resulting cronjobs into the crontab.
								****************************************************************************/
									include('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
// 									include('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
								/****************************************************************************
									Delete the subscribers previous account while retaining his $passhash.
								****************************************************************************/
								$sql = "DELETE FROM $tbl_name WHERE guid=?";
								$mysqli_stmt->prepare($sql);
								$mysqli_stmt->bind_param("s", $old_guid);
								$mysqli_stmt->execute();
								$mysqli_stmt->free_result();
								echo "<p>Congratulations, $username!  You are now a paid subscriber and will begin receiving regularly scheduled pages of Mount Cambitas - The Story of Real Money.  Your first delivery will be the first page of Chapter 2.  If this is not to your liking you may change the starting page of your delivery in the section entitled Reschedule, Pause Delivery, Page Reset, and Free Download on this page.</p>";
							} else {
								echo "<p>Please enter a suitable time schedule.<br />You may change it at any time in the future.</p>";
							}
						}
					}   else {
						echo "<p>Please enter the correct username and email address.</p>";
					}			
				}
			}  else {
				echo "<p>Please enter a correct username and email address -- the same with which you originally subscribed.</p>";
			}	
		}  else {
			echo "<p>Please enter the passphrase that was sent to you in the welcome email when you first subscribed.</p><p>If you have lost or misplaced it, you may request a new passphrase in the Toolbox section.</p>";
		}
	}
?>