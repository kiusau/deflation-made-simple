<?php
	/****************************************************************************
		Error Handling
	****************************************************************************/
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
	/****************************************************************************
		Set up the TiBeN Namespace for the PHP Include
	****************************************************************************/
	require_once("/home/thege0/vendor/autoload.php");
	use TiBeN\CrontabManager\CrontabJob;
	use TiBeN\CrontabManager\CrontabRepository;
	use TiBeN\CrontabManager\CrontabAdapter;
	/****************************************************************************
		Turn on and turn off specific cronjobs.
	****************************************************************************/
	if (isset($_GET['guid'])) {
		$guid = filter_var($_GET['guid'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH);	
		$crontabRepository = new CrontabRepository(new CrontabAdapter());		
		$cronjobs = $crontabRepository->findJobByRegex("/$guid/");
		foreach($cronjobs as $cronjob) {
			foreach ($cronjob as $key => $value) {
				if ($key == 'enabled') {
					if ($value == false) {
						$cronjob->setEnabled(true);
					} else if ($value == true) {
						$cronjob->setEnabled(false);
					}
				}
			}
		}
		$crontabRepository->persist();
		$cronjobs = $crontabRepository->findJobByRegex("/$guid/");
	}
?>