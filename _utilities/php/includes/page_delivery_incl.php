<?php
/*
** Filename: page_delivery_incl.php
** Location: /Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes
** Corresponding Files:  Included by ~/cambitas/_utilities/php/rm.php.
** Creation Date: Saturday, 9 April 2022
** Author: Hashimori Iwato
** Brief Description:  This page serves several purposes:
** 1) Create a cronjob representing the reader's desired schedule and send it to the CRON engine for processing.
** 2) Process a call from the CRON engine to send a page  
*/
	/****************************************************************************
		Set up the TiBeN Namespace for the PHP Include
	****************************************************************************/
// 	require_once("/Users/iwato/vendor/autoload.php");
	require_once("/home/thege0/vendor/autoload.php");
	use TiBeN\CrontabManager\CrontabJob;
	use TiBeN\CrontabManager\CrontabRepository;
	use TiBeN\CrontabManager\CrontabAdapter;
	/****************************************************************************
		Enter the Reader's Schedule as a Cronjob into the OS Crontab
	****************************************************************************/
	If (isset($guid) && isset($email) && isset($cronjobs) && isset($timezone)) {
		/****************************************************************************
			Extract the day string from the $cronjobs array and convert it into
			an array for timezone adjustment.			
		****************************************************************************/
		function createDayArray($day) {
			if (is_string($day)) {
				if ($day == "MON-FRI") {
					$day_arr = array(1,2,3,4,5);
				} else if ($day == "SUN-SAT") {
					$day_arr = array(0,1,2,3,4,5,6);
				} else {
					$day_arr = explode(",", $day);
					array_walk($day_arr, function($key,$value) {
						$value = (int) $value;						
					});						
				}
			} else {
				$day_arr[] = $day;
			}
			return $day_arr;				
		}
		/****************************************************************************
			Convert Hours and DayOfWeek to UTC Time
		****************************************************************************/
		function setDayAndTime($day, $hour, $timezone) {
			$day_time['day'] = [];
			$tz = new DateTimeZone($timezone);
			$now = new DateTime("now", $tz);
			$offset = $now->getOffset()/(60*60);
			if ($offset <= 0) {
				$diff = $hour - $offset;
				if ($diff < 24) {
					$day_time['hour'] = $diff;
				} else if ($diff >= 24) {
					$day_time['hour'] = $diff - 24;
				}
				if ((($hour - $offset) - 24) == $day_time['hour']) {
					$day_time['day'] = $day + 1;
					if ($day_time['day'] == 7) {
						$day_time['day'] = 0;
					}
				} else if ($hour - $offset == $day_time['hour']) {
					$day_time['day'] = $day;
				}
			} else if ($offset > 0) {
				$diff = $hour - $offset;
				if ($diff >= 0) {
					$day_time['hour'] = $diff;
				} 
				else if ($diff < 0) {
					$day_time['hour'] = $diff + 24;
				}
				if ($hour - $offset == $day_time['hour']) {
					$day_time['day'] = $day;
				} else if (24 - $offset + $hour == $day_time['hour']) {
					$day_time['day'] = $day - 1;
					if ($day_time['day'] < 0) {
						$day_time['day'] = 6;
					}
				} 
			}
			return $day_time;
		}
		/****************************************************************************
			Convert the dayofWeek variable into a comma separated numerical string
		****************************************************************************/
		function recreateCronitem ($cronitem, $timezone) {
			$days_time = [];
			foreach($cronitem as $key => $value) {
				if ($key == 'hours') {
					$hour = $value;
				}
				if($key == 'dayOfWeek') {
					$days = createDayArray($value);
					foreach ($days as $day_item) {
						$days_time[] = setDayAndTime($day_item, $hour, $timezone);						
					}
				}
			}
// 			print_r($days_time);
			$select_arr = [];
			foreach ($days_time as $select) {
				foreach ($select as $key => $value) {
					if ($key == "day") {
						$select_arr[] = $value;
					}
					if ($key == "hour") {
						$hour = $value;
					}
				}
			}
			$day_str = implode(",", $select_arr);
			$cronitem["dayOfWeek"] = $day_str;
			$cronitem["hours"] = $hour;
			return $cronitem;
		}
		/****************************************************************************
			Create the Cronjob from the modified day and hour data.
		****************************************************************************/
		$crontabRepository = new CrontabRepository(new CrontabAdapter());
		foreach ($cronjobs as $cronitem) {
			$cronitem = recreateCronitem($cronitem, $timezone);
			$cronjob = new CrontabJob();
			foreach ($cronitem as $name => $value) {
				$cronjob->setTaskCommandLine("/usr/local/bin/ea-php74 /home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/page_delivery.php guid=$guid email=$email");
// 				$cronjob->setTaskCommandLine("/usr/local/bin/php /Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/page_delivery.php guid=$guid email=$email");				
				$cronjob->setEnabled(true);
				$cronjob->setComments($guid);
// 				$cronjob->setShortcut(">/dev/null 2>&1");
				if ($name == 'minutes') {
					$cronjob->setMinutes($value);
				}
				if ($name == 'hours') {
// 					if ($value == 24) {
// 						$value = 0;
// 					}
					$cronjob->setHours($value);
				}
				if ($name == 'dayOfWeek') {
					$cronjob->setDayOfWeek($value);
				}
			}								
// 			print_r($cronjob);
// 			$cronjob = clone $cronjob;
// 			$cronjob->setEnabled(false);
			$crontabRepository->addJob($cronjob);
		}
		$crontabRepository->persist();
	}
?>
