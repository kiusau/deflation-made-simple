<?php
	if (isset($_GET["file"])) {
		$file = filter_var($_GET["file"], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		header("Content-Type: application/octet-stream");  
		$file = "https://cambitas.spiritof2021.online/_documents/" . $file . ".pdf";  
		header("Content-Disposition: attachment; filename=" . urlencode($file));   
		header("Content-Type: application/download");
		header("Content-Description: File Transfer");            
		header("Content-Length: " . filesize($file));  
		flush(); // This doesn't really matter.  
		$fp = fopen($file, "r");
		while (!feof($fp)) {
			echo fread($fp, 65536);
			flush(); // This is essential for large downloads
		}   
		fclose($fp);
	}
?>

