<?php
/**********************************************************************
**  Function Name:  strstr_after()
**  Author:  Roddy A. Stegemann
**  Date:  26 January 2018
**  Brief Description:  Finds the first occurrence of the designated substring and returns everything thereafter.
**********************************************************************/
function strstr_after($haystack, $needle, $case_insensitive = false) {
	$strpos = ($case_insensitive) ? 'stripos' : 'strpos';
	$pos = $strpos($haystack, $needle);
	if (is_int($pos)) {
		return substr($haystack, $pos + strlen($needle));
	}
	return $pos;
}
?>