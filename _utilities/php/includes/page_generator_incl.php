<?php
/**
/* Author:  Roddy A. Stegemann
/* Date of Creation:  20 Apirl 2022
/* Description:  Generate the welcome email and send it to subscribers whose identity has been confirmed.
/* Linkage:  
** Included into email_verify.php after the subscriber's identity has been confirmed according to payment type.  
** Include class.page_data.php for the purpose of obtaining data from the subsribers and repository datatables that are specific to the confirmed user.
** Include simple_page.php to prepare the email template with the data obtained from class.page_data.php.
** Access email_template_initial.txt and email_template_initial.html and fill the tags with the data prepared by simple.page.php.
** Include the PHPMailer namespace in preparation for mailing.
/* Data Input:  Data received from email_verifiy.php into which this page is included, as well as that obtained via class.page_data.php.
**/
	/************************************************************
	Set up PHP error reporting.
	*************************************************************/
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	/************************************************************
	Fetch the appropirate banner image.
	*************************************************************/
	$image_link = "<img src='cid:aveverum_banner' max-width='600' width:100% height='145' style='margin:0; padding:0; border:none; display:block;' border='0' alt='The Ave Verum Banner' />";
	$tempage_html = '';
	$tempage_text = '';
	/************************************************************
	Obtain page data from repository and subcriber data tables.
	*************************************************************/
	require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php');
// 	require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/classes/class.page_data.php');
	if ($pageno == -1){
		/************************************************************
		Obtain subscriber and page data for fully paid patron.
		*************************************************************/
		$page_data = new PageData($mysqli_obj, $guid, $pageno);
		$subscriber_data = $page_data->get_subscriber_data($guid);
		$username = $subscriber_data['username'];
		$email = $subscriber_data['email'];
		$chapter = $subscriber_data['chapter'];
		$timezone = $subscriber_data['timezone'];
		$message_html = $page_data->get_page_data(0, "html");
		$message_html = $message_html['html'];
// 		$message_text = $page_data->get_page_data(0, "text");
// 		$message_text = $message_text['text'];
		$html = $subscriber_data['html'];
// 		$text = $subscriber_data['text'];
		/************************************************************
		Create the $dte_time sring.
		*************************************************************/
		$timestamp = time();
		$date_time = new DateTime("now", new DateTimeZone($timezone));
		$date_time->setTimestamp($timestamp);
		$date = $date_time->format('D, d M Y, H:i:s');
		/************************************************************
		Create the $tags Array and fill the text and html templates.
		*************************************************************/
		require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 		require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
		if (isset($text)) {
// 		if (!empty($text)) {
// 			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// // 			$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 			$tempage_text = new SimplePage($template);
// 			$tags_text = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'passphrase' => $password, 'msg_paytype' => $text, 'message' => $message_text);
// 			$tempage_text->replace_tags($tags_text);
		}
		if (!empty($html)) {
			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.html';
// 			$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.html';
			$tempage_html = new SimplePage($template);
			$tags_html = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'passphrase' => $password, 'msg_paytype' => $html, 'message' => $message_html, 'image' => $image_link);
			$tempage_html->replace_tags($tags_html);
		}
	} else if ($pageno == -2) {
		/************************************************************
		Obtain subscriber and page data for unpaid, scheduled patron.
		*************************************************************/
		$page_data = new PageData($mysqli_obj, $guid, $pageno);
		$subscriber_data = $page_data->get_subscriber_data($guid);
		$username = $subscriber_data['username'];
		$email = $subscriber_data['email'];
		$chapter = $subscriber_data['chapter'];
		$timezone = $subscriber_data['timezone'];
		$message_html = $page_data->get_page_data(0, "html");
		$message_html = $message_html['html'];
// 		$message_text = $page_data->get_page_data(0, "text");		
// 		$message_text = $message_text['text'];
		$html = $subscriber_data['html'];
// 		$text = $subscriber_data['text'];
		/************************************************************
		Create the $date_time sring.
		*************************************************************/
		$timestamp = time();
		$date_time = new DateTime("now", new DateTimeZone($timezone));
		$date_time->setTimestamp($timestamp);
		$date = $date_time->format('D, d M Y, H:i:s');
		/************************************************************
		Create the $tags Array and fill the text and html templates.
		*************************************************************/
		require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 		require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
		if (isset($text)) {
// 		if (!empty($text)) {
// 			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 			$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 			$tempage_text = new SimplePage($template);
// 			$tags_text = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'passphrase' => $password, 'msg_paytype' => $text, 'message' => $message_text);
// 			$tempage_text->replace_tags($tags_text);
		}
		if (!empty($html)) {
			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.html';
// 			$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.html';
			$tempage_html = new SimplePage($template);
			$tags_html = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'passphrase' => $password, 'msg_paytype' => $html, 'message' => $message_html, 'image' => $image_link);
			$tempage_html->replace_tags($tags_html);
		}
	} else if ($pageno == -3) {
		/************************************************************
		Obtain subscriber and page data for free download.
		*************************************************************/
		$page_data = new PageData($mysqli_obj, $guid, $pageno);
		$subscriber_data = $page_data->get_subscriber_data($guid);
		$username = $subscriber_data['username'];
		$email = $subscriber_data['email'];
		$chapter = $subscriber_data['chapter'];
		$timezone = $subscriber_data['timezone'];
		$message_html = $page_data->get_page_data(0, "html");
		$message_html = $message_html['html'];
// 		$message_text = $page_data->get_page_data(0, "text");		
// 		$message_text = $message_text['text'];
		$html = $subscriber_data['html'];
// 		$text = $subscriber_data['text'];
		/************************************************************
		Create the $date_time sring.
		*************************************************************/
		$timestamp = time();
		$date_time = new DateTime("now", new DateTimeZone($timezone));
		$date_time->setTimestamp($timestamp);
		$date = $date_time->format('D, d M Y, H:i:s');
		/************************************************************
		Create the $tags Array and fill the text and html templates.
		*************************************************************/
		require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 		require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
		if (isset($text)) {
// 		if (!empty($text)) {
// 			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 			$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.txt';
// 			$tempage_text = new SimplePage($template);
// 			$tags_text = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'passphrase' => $password, 'msg_paytype' => $text, 'message' => $message_text);
// 			$tempage_text->replace_tags($tags_text);
		}
		if (!empty($html)) {
			$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_initial.html';
// 			$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_initial.html';
			$tempage_html = new SimplePage($template);
			$tags_html = array('date' => $date, 'timezone' => $timezone, 'name' => $username, 'passphrase' => $password, 'msg_paytype' => $html, 'message' => $message_html, 'image' => $image_link);
			$tempage_html->replace_tags($tags_html);
		}
	} else if ($newphrase == 1) {
		/************************************************************
		Create the $date_time sring.
		*************************************************************/
		$timestamp = time();
		$date_time = new DateTime("now", new DateTimeZone($timezone));
		$date_time->setTimestamp($timestamp);
		$date = $date_time->format('D, d M Y, H:i:s');
		/************************************************************
		Create the $tags Array and fill the text and html templates.
		*************************************************************/
		require_once('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
// 		require_once('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/simple_page.php');
		$template = '/home/thege0/public_html/spiritof2021.online/cambitas/email_template_newphrase.html';
// 		$template = '/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/email_template_newphrase.html';
		$tempage_html = new SimplePage($template);
		$tags_html = array('date' => $date, 'timezone' => $timezone, 'name' => $subscriber, 'passphrase' => $password, 'image' => $image_link);
		$tempage_html->replace_tags($tags_html);
	}
	/************************************************************
	Prepare the email for mailing and send.
	*************************************************************/
	require_once('/home/thege0/vendor/autoload.php');
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;
	$mail = new PHPMailer(true);
	/************************************************************/
	try {
// 		$mail->SMTPDebug = SMTP::DEBUG_SERVER;
		$mail->SMTPDebug = 0;
		$mail->isSMTP();
		$mail->SMTPAuth   = true;
// 		$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
		$mail->Host = "nwurq132.hostpapavps.net";
		$mail->Port = 587;
		$mail->Username = "admin@spiritof2021.online";
		$mail->Password = "FCw2UxG7jmeX7L8";
// 		$mail->Host = "0.0.0.0";
// 		$mail->Port       = 1025; 
// 		$mail->Username   = 'iwato@jupiter.local';         
// 		$mail->Password   = 'nudge.Liberty+';                   
		/************************************************************/
		$mail->setFrom('admin@spiritof2021.online', 'Spirit of 2021');
		$mail->addAddress($email, $subscriber);
		$mail->addReplyTo('admin@spiritof2021.online', 'Spirit of 2021');
		/************************************************************/
		$mail->CharSet = 'UTF-8';
		$mail->Priority = 1;
		$mail->addEmbeddedImage('../../_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
		/************************************************************/
		$mail->isHTML(true);
		$mail->Subject = 'Spirit of 2021 - Mount Cambitas';
		$mail->Body = htmlspecialchars_decode($tempage_html->output());
// 		$mail->msgHTML = $tempage_html->output();
		$mail->AltBody = "Hi, %name%! Welcome to Mount Cambitas - The Story of Real Money.  Please turn on HTML formatting so that you might enjoy your new subscription to its fullest.  In liberty, or not at all, Roddy A. Stegemann";
		/************************************************************/
		$mail->send();
// 		echo 'Message has been sent';
	} catch (Exception $e) {
		echo "Message could not be sent. Mailer Error (1): " . $e->errorMessage();
	} catch (\Exception $e) {
		echo "Message could not be sent. Mailer Error (2): " . $e->getMessage();
	}
?>
