<?php
/**
/* Author:  Roddy A. Stegemann
/* Date of Creation:  18 March 2022
/* Description:  1) Receive from the user his username, email, and passhash in an effort to verify his device identity and activate his account by resetting or not resetting his payment status depending on his selection before the confirmation email was sent. Provide the user with a confirmation link that informs him about the success or failure of his transaction.  Generates his first correspondence subsequent to device verification and the activation of his account.
/* Linkage:  
** Include class.papaconnect.php in order to access the spirit_db database.  
** Include page_generator_incl.php in order to generate the welcome page after verification.  
** Call multipurpose_confirmation.php in order to inform the user that his account has been activated.
/* Data Input:  Receive data sent from confirmation email produced by rm.php.
**/
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
	/****************************************************************************
		Create MySQLi object for query, matching (verification) and upadate
	****************************************************************************/
	include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
	$papa_connect = new PapaConnect();
	$mysqli_obj = $papa_connect->get_mysqli_obj();
// 	include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 	$iwato_connect = new IwatoConnect();
// 	$mysqli_obj = $iwato_connect->get_mysqli_obj();
	/****************************************************************************
		Create MySQLi object for query, matching (verification) and upadate
	****************************************************************************/
	$link = 'mailto:admin@spiritof2021.online?Subject=Spirit%20of%202021%20-%20Account%20Verification';	
	if(isset($_GET['payment']) && isset($_GET['guid'])) {
		/********************************************************************************
			Prepare the query string sent by the confirmation email for databse entry
		********************************************************************************/
		$subscriber = '';
		$email = '';
		$payment = $mysqli_obj->real_escape_string($_GET['payment']);
		$pageno = 0;
		$guid = $mysqli_obj->real_escape_string($_GET['guid']);
		/********************************************************************************
			Generate a random password and passhash
		********************************************************************************/
		function random_password() {
			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			$pass = array();
			$alphaLength = strlen($alphabet) - 1;
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass);
		}
		$password = random_password();
		$hash = password_hash($password, PASSWORD_DEFAULT);
		/********************************************************************************
			Obtain subscriber info based on criteria received from confirmation mail.
		********************************************************************************/
		$tbl_name = 'spirit_db.subscribers';
// 		$tbl_name = 'spirit.subscribers';
		$sql_select = "SELECT username, email FROM $tbl_name WHERE guid=?";
		$mysqli_stmt = $mysqli_obj->stmt_init();
		$mysqli_stmt->prepare($sql_select);
		$mysqli_stmt->bind_param("s", $guid);
		$mysqli_stmt->execute();
		$mysqli_stmt->store_result();
		$match = $mysqli_stmt->num_rows;
		/************************************************************************************
			Where at least one match has been found update database, send welcome email,
			and create confirmation webpage that notifies the user of the pending email.
		************************************************************************************/
		if($match > 0){
			$meta = $mysqli_stmt->result_metadata();
			while ($field = $meta->fetch_field()) {
				$params[] = &$row[$field->name];
			}
			call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
			while ($mysqli_stmt->fetch()) {
				foreach($row as $key => $val) {
					$c[$key] = $val;
				}
				$prelim_result[] = $c;
			}
			foreach ($prelim_result as $arr) {
				foreach ($arr as $name => $value){
					$page_results[$name] = $value;
				}
			}
			$mysqli_stmt->free_result();
			$subscriber = $page_results['username'];
			$email = $page_results['email'];
			if ($payment == 0) {
				$sql_update = "UPDATE " . $tbl_name . " SET payment='1', pageno='-3', passhash=? WHERE guid=? AND payment='0'";
				$mysqli_stmt->prepare($sql_update);
				$mysqli_stmt->bind_param("ss", $hash, $guid);
				$mysqli_stmt->execute();
				$mysqli_stmt->free_result();
				$msg = "<p>Congratulations, " . "<span class='name'>" . $subscriber . "</span>" . "!<br />Your account has been activated.</p><p>Please check your inbox for an email containing your passphrase, a brief set of instructions, and a formal introduction to <em>Mount Cambitas:  The Story of Real Money</em>.</p>";
				$pageno = -3;
				/************************************************************************************
					Clean database of extraneous user accounts from same user.
				************************************************************************************/
				$sql_clean = "DELETE FROM $tbl_name WHERE username=? AND email=? AND NOT guid=?";
				$mysqli_stmt->prepare($sql_clean);
				$mysqli_stmt->bind_param("sss", $subscriber, $email, $guid);
				$mysqli_stmt->execute();
				$mysqli_stmt->free_result();
				/************************************************************************************
					Send welcome page.
				************************************************************************************/
				include_once("/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
// 				include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
			} else if ($payment == 1) {			
				$sql_update = "UPDATE " . $tbl_name . " SET payment='1', pageno='-2', passhash=? WHERE guid=? AND payment='0'";
				$mysqli_stmt->prepare($sql_update);
				$mysqli_stmt->bind_param("ss", $hash, $guid);
				$mysqli_stmt->execute();
				$mysqli_stmt->free_result();
				$msg = "<p>Congratulations, " . "<span class='name'>" . $subscriber . "</span>" . "!<br />Your account has been activated.</p><p>Please check your inbox for an email containing your passphrase, a brief set of instructions, and a formal introduction to <em>Mount Cambitas:  The Story of Real Money</em>.</p>";
				$pageno = -2;
				/************************************************************************************
					Clean database of extraneous user accounts from same user.
				************************************************************************************/
				$sql_clean = "DELETE FROM $tbl_name WHERE username=? AND email=? AND NOT guid=?";
				$mysqli_stmt->prepare($sql_clean);
				$mysqli_stmt->bind_param("sss", $subscriber, $email, $guid);
				$mysqli_stmt->execute();
				$mysqli_stmt->free_result();
				/************************************************************************************
					Send welcome page.
				************************************************************************************/
				include_once("/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
// 				include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
			} else if ($payment == 2) {
				$sql_update = "UPDATE " . $tbl_name . " SET payment='2', pageno='-1', passhash=? WHERE guid=? AND payment='0'";
				$mysqli_stmt->prepare($sql_update);
				$mysqli_stmt->bind_param("ss", $hash, $guid);
				$mysqli_stmt->execute();
				$mysqli_stmt->free_result();
				$msg = "<p>Congratulations, " . "<span class='name'>" . $subscriber . "</span>" . "!<br />Your account has been activated.</p><p>Please check your inbox for an email containing your passphrase, a brief set of instructions, and a formal introduction to <em>Mount Cambitas:  The Story of Real Money</em>.</p>";
				$pageno = -1;
				/************************************************************************************
					Clean database of extraneous user accounts from same user.
				************************************************************************************/
				$sql_clean = "DELETE FROM $tbl_name WHERE username=? AND email=? AND NOT guid=?";
				$mysqli_stmt->prepare($sql_clean);
				$mysqli_stmt->bind_param("sss", $subscriber, $email, $guid);
				$mysqli_stmt->execute();
				$mysqli_stmt->free_result();
				/************************************************************************************
					Send welcome page.
				************************************************************************************/
				include_once("/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
// 				include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
			}
		} else {
			$msg = "<p>Hi, " . "<span class='name'>" . $subscriber . "</span>" . "!</p><p>Sorry, but we were unable to verify you as the recipient of a new account.</p><p>Are you sure that you are not already subscribed?</p><p>Does your email address match the one to which the verification email was sent?</p><p>Please contact the <a class='link_style' href='" . $link . "' title='The Spirit of 2021 Webmaster' target='_top'>webmaster</a> at the Spirit of 2021 to insure that your account has been activated.</p><p>In your correspondence include your newly created Spirit of 2021 username and the email address, if you still have it, with which you signed up for a new Spirit of 2021 account.</p><p>Thank you for your additional effort! You will not be disappointed.</p>";
		}
	} else if (isset($_GET['newphrase']) && isset($_GET['guid'])) {
		/********************************************************************************
			Prepare the query string sent by the confirmation email for databse entry
		********************************************************************************/
		$subscriber = '';
		$email = '';
		$pageno = 0;
		$newphrase = $mysqli_obj->real_escape_string($_GET['newphrase']);
		$guid = $mysqli_obj->real_escape_string($_GET['guid']);
		/********************************************************************************
			Generate a random password and passhash
		********************************************************************************/
		function random_password() {
			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			$pass = array();
			$alphaLength = strlen($alphabet) - 1;
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass);
		}
		$password = random_password();
		$hash = password_hash($password, PASSWORD_DEFAULT);
		/********************************************************************************
			Obtain subscriber info based on criteria received from confirmation mail.
		********************************************************************************/
		$tbl_name = 'spirit_db.subscribers';
// 		$tbl_name = 'spirit.subscribers';
		$sql_select = "SELECT username, email, timezone FROM $tbl_name WHERE guid=?";
		$mysqli_stmt = $mysqli_obj->stmt_init();
		$mysqli_stmt->prepare($sql_select);
		$mysqli_stmt->bind_param("s", $guid);
		$mysqli_stmt->execute();
		$mysqli_stmt->store_result();
		$match = $mysqli_stmt->num_rows;
		/************************************************************************************
			Where at least one match has been found update database, send new passphrase,
			and create confirmation webpage that notifies the user of the pending email.
		************************************************************************************/
		if($match > 0){
			$meta = $mysqli_stmt->result_metadata();
			while ($field = $meta->fetch_field()) {
				$params[] = &$row[$field->name];
			}
			call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
			while ($mysqli_stmt->fetch()) {
				foreach($row as $key => $val) {
					$c[$key] = $val;
				}
				$prelim_result[] = $c;
			}
			foreach ($prelim_result as $arr) {
				foreach ($arr as $name => $value){
					$page_results[$name] = $value;
				}
			}
			$mysqli_stmt->free_result();
			$subscriber = $page_results['username'];
			$email = $page_results['email'];
			$timezone = $page_results['timezone'];
			$sql_update = "UPDATE " . $tbl_name . " SET passhash=? WHERE guid=?";
			$mysqli_stmt->prepare($sql_update);
			$mysqli_stmt->bind_param("ss", $hash, $guid);
			$mysqli_stmt->execute();
			$mysqli_stmt->free_result();
			$msg = "<p>Congratulations, " . "<span class='name'>" . $subscriber . "</span>" . "!<br />Your new passphrase is now active.</p><p>Please check your inbox for an email containing your new passphrase.  And, welcome back to <em>Mount Cambitas:  The Story of Real Money</em>.</p>";
			/************************************************************************************
				Clean database of extraneous user accounts from same user.
			************************************************************************************/
			$sql_clean = "DELETE FROM $tbl_name WHERE username=? AND email=? AND NOT guid=?";
			$mysqli_stmt->prepare($sql_clean);
			$mysqli_stmt->bind_param("sss", $subscriber, $email, $guid);
			$mysqli_stmt->execute();
			$mysqli_stmt->free_result();
			/************************************************************************************
				Send welcome page.
			************************************************************************************/
			include_once("/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
// 			include_once("/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_generator_incl.php");
		} else {
			$msg = "<p>Hi, " . "<span class='name'>" . $subscriber . "</span>" . "!</p><p>Sorry, but we were unable to verify you as the owner of the account.</p><p>Does your email address match the one to which the verification email was sent?</p><p>Please contact the <a class='link_style' href='" . $link . "' title='The Spirit of 2021 Webmaster' target='_top'>webmaster</a> at the Spirit of 2021 in order to discover what has happened.</p><p>In your correspondence include whatever additional information about your account that you can provide including the approximate date of your original subscription.</p><p>Thank you for your additional effort! You will not be disappointed.</p>";
		}
	}
	header("location: ../../multipurpose_confirmation.php?name=$subscriber&msg=$msg");
?>