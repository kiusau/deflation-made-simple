<?php
	class SimplePage {
		protected $page;
		function __construct($template) {
			if (file_exists($template)) {
				$this->page = file_get_contents($template);
			} else {
				die("Template file $template not found.");
			}
		}
		function parse($file) {
			ob_start();
			include($file);
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;
		}
		function replace_tags($tags) {
			if (sizeof($tags) > 0) {
				foreach ($tags as $tag => $data) {
					$replace = '%\%' . $tag . '\%%';
					$data = (file_exists($data)) ? join("", file($data)) : $data;
					$this->page = preg_replace($replace, $data, $this->page);
				}
			} else {
				die("No tags designated for replacement.");
			}
		}	
		function fill() {
		  echo $this->page;
  		}
		function output() {
		  return $this->page;
  		}
	}
?>