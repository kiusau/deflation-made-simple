<?php
/*
**File Name:  schdmod.php
**Creation Date:  Monday, 25 April 2022
**Author:  Roddy A. Stegemann
**Brief Description:  Service the recreate_entry form data entered in scheduling.html and served by scheduling.js by the formThree handler.
*/
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
/****************************************************************************
Create response to AJAX request for User GUID and schedule confirmation.
****************************************************************************/
	require_once("/home/thege0/vendor/autoload.php");
// 	require_once("/Users/iwato/vendor/autoload.php");
	use Ramsey\Uuid\Uuid;
	use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
	use TiBeN\CrontabManager\CrontabJob;
	use TiBeN\CrontabManager\CrontabRepository;
	use TiBeN\CrontabManager\CrontabAdapter;
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	if(isset($_POST['jsonSet'])) {
		if (is_array($_POST['jsonSet'])){
			$jS_arr = $_POST['jsonSet'];
			$userTZ = filter_var($_POST['timezone'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
			$uuid = Uuid::uuid4();
			$uuid->toString();
			$schd_arr[0]=$uuid;
	// 		$crontabJob = new CrontabJob();
	// 		$crontabJob->setComments($uuid);
			$crontabJob;
			$crontabJobs = [];
			$infoPacket = [];
			$days =[];
			$cronDays = [];
			$dayStr = '';
			$cronStr = '';
			$time = '';
			$cronTime = 0;
			$hour = '';
			$min = '';
	// 		$result =[];
			function setDay($dayMultiple) {
				$dayStr = '';
				$cronStr = '';
				$result = [];
				$resultStrs = [];
				foreach ($dayMultiple as $dayValue) {
					$dayValue = filter_var($dayValue, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
					switch ($dayValue) {
						case 'every':
						$day = 'Everyday';
						$cronDay = "SUN-SAT";
						break;
						case 'week':
						$day = 'Weekdays';
						$cronDay = "MON-FRI";
						break;
						case 'mon':
						$day = 'Monday';
						$cronDay = 1;
						break;
						case 'tue':
						$day = 'Tuesday';
						$cronDay = 2;
						break;
						case 'wed':
						$day = 'Wednesday';
						$cronDay = 3;
						break;
						case 'thu':
						$day = 'Thursday';
						$cronDay = 4;
						break;
						case 'fri':
						$day = 'Friday';
						$cronDay = 5;
						break;
						case 'sat':
						$day = 'Saturday';
						$cronDay = 6;
						break;
						case 'sun':
						$day = 'Sunday';
						$cronDay = 0;
						break;
						default:
							echo "Please select a day!";
					}
					$days[] = $day;
					$cronDays[] = $cronDay;
				}
				if (count($days) == 1) {
					$dayStr = $days[0];
					$cronStr = $cronDays[0];
				} else if (count($days) == 2) {
					$dayStr = $days[0] . " and " . $days[1];
					$cronStr = $cronDays[0] . "," . $cronDays[1];
				} else {
					for($i=0; $i < (count($days)-1); $i++) {
						$dayStr .= $days[$i] . ", ";
						$cronStr .= $cronDays[$i] . ",";
					}
					$dayStr .= " and " . $days[count($days)-1];
					$cronStr .= $cronDays[count($days)-1];
				}
				$resultStrs = [$dayStr, $cronStr];
				return $resultStrs;
			}
			/**********************************************************************************
				Create an Array for schedule entries with multiple days for the same time
			**********************************************************************************/
			function setDayRun($value3) {
				$dayResult = [];
				return $dayResult = setDay($value3);
			}
			/**********************************************************************************
				Assemble multiple schedule entries for the same user.
			**********************************************************************************/
			foreach ($jS_arr as $key1 => $value1) {
				$crontabJob = new CrontabJob();
				$crontabJob->setEnabled(true);
				$crontabJob->setComments($uuid);
				foreach($value1 as $value2 => $value3) {
					if (is_array($value3)) {
						$result = setDayRun($value3);
						$dayStr = $result[0];
						$cronStr = $result[1];
						$crontabJob->setDayOfWeek($cronStr);
					}
					if ($value2 == 'time') {
						$time = filter_var($value3, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
						if ($time == 'AM') {
							$cronTime = 0;
						}
						if ($time == 'PM') {
							$cronTime = 12;
						}
					}
					if ($value2 == 'hour') {
						$hour = filter_var($value3, FILTER_VALIDATE_INT);
						$cronHour = $value3 + $cronTime;
						$crontabJob->setHours($cronHour);
					}
					if ($value2 == 'min') {
						$min = filter_var($value3, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
						$crontabJob->setMinutes($min);
					}
				}
				$schd_arr[] = $dayStr . ' at ' . $hour . ':' . $min . ' ' . $time;
				$crontabJobs[] = $crontabJob;
				unset($crontabJob);
			}
			/********************************************************************
				Create a JSON object to repond to AJAX call from scheduling.js
			*********************************************************************/
			$info_pack = array($crontabJobs, $schd_arr, "timezone" => $userTZ);
	// 		echo json_encode($crontabJobs);
	// 		echo json_encode($schd_arr);
			echo json_encode($info_pack);
		}
	}
	/**********************************************************************************
		Reset the subscriber's schedule as determined by the subscriber
	**********************************************************************************/
	if (!empty($_POST['passphrase']) && isset($_POST['guid'])) {
		/****************************************************************************
			Filter the subscriber's contact and other info sent by the AJAX routine
		****************************************************************************/
		$pageno = filter_var($_POST['pageno'], FILTER_VALIDATE_INT);
		$guid = filter_var($_POST['guid'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH);
		$username = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$email = filter_var($_POST['email'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_SANITIZE_EMAIL);
		$passphrase = filter_var($_POST['passphrase'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$minutes = 0;
		$hour = 0;
		$day_of_week = '';
		$old_guid = '';
		$page_limit = 3;
		$current_page = 0;
		$timezone = filter_var($_POST['timezone'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		/*********************************************************************************
			Filter the contents of the prepared cronjob returned with the payment info
		*********************************************************************************/
		if (isset($_POST['cronjobs'])) {
			$cronjobs = $_POST['cronjobs'];
			function filterCronjobs($cronjobs) {
				$filters = array(
					'enabled' => FILTER_VALIDATE_BOOLEAN,
					'minutes' => FILTER_VALIDATE_INT,
					'hours' => FILTER_VALIDATE_INT,
					'dayOfMonth' => FILTER_VALIDATE_INT,
					'months' => FILTER_VALIDATE_INT,
					'dayOfWeek' => "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH",
					'taskCommandLine' => "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH",
					'comments' => "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH",
					'shortCut' =>  "FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH"
				);
				foreach($cronjobs as $key => $value) {
					$filtered_cronjobs[] = filter_var_array($value, $filters);
				}
				return $cronjobs;
			}
			$cronjobs = filterCronjobs($cronjobs);
		}
		/****************************************************************************
			Create MySQLi object for query, matching (verification) and upadate
		****************************************************************************/
		include_once("../../../_utilities/php/classes/class.papaconnect.php");
		$papa_connect = new PapaConnect();
		$mysqli_obj = $papa_connect->get_mysqli_obj();	
// 		include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 		$iwato_connect = new IwatoConnect();
// 		$mysqli_obj = $iwato_connect->get_mysqli_obj();
		/********************************************************************************
			Obtain subscriber info based on criteria received from confirmation mail.
		********************************************************************************/
		$tbl_name = 'spirit_db.subscribers';
// 		$tbl_name = 'spirit.subscribers';
		$sql_select = "SELECT passhash, payment, pageno, guid FROM $tbl_name WHERE username=? AND email=?";
		$mysqli_stmt = $mysqli_obj->stmt_init();
		$mysqli_stmt->prepare($sql_select);
		$mysqli_stmt->bind_param("ss", $username, $email);
		if ($mysqli_stmt->execute()) {
			$meta = $mysqli_stmt->result_metadata();
			while ($field = $meta->fetch_field()) {
				$params[] = &$row[$field->name];
			}
			call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
			while ($mysqli_stmt->fetch()) {
				foreach($row as $key => $val) {
					$c[$key] = $val;
				}
				$prelim_result[] = $c;
			}
			if ($prelim_result) {
				foreach ($prelim_result as $arr) {
					foreach ($arr as $name => $value){
						if ($name == "passhash") {
							$passhash = $value;
						}
						if ($name == "payment") {
							$payment = $value;
						}
						if ($name == "pageno") {
							$current_page = $value;
						}
						if ($name == "guid") {
							$old_guid = $value;
						}
					}
				}	
				/********************************************************************************
					Determine payment status and act accordingly.
				********************************************************************************/
				$mysqli_stmt->free_result();
				if (password_verify($passphrase, $passhash)) {
					if ((($payment == 1) && ($current_page < $page_limit)) || $payment == 2 ) {
						foreach ($cronjobs as $key) {
							foreach ($key as $name => $value) {
								if ($name == 'minutes') {
									$minutes = $value;
								}
								if ($name == 'hours') {
									$hour = $value;
								}
								if ($name == 'dayOfWeek') {
									$day_of_week = $value;
								}
							}					
							/****************************************************************************
								Insert the individual cronjobs into the database.
							****************************************************************************/
							$query = "INSERT INTO $tbl_name (guid, username, email, passhash, payment, pageno, minutes, hour, day_of_week, timezone) VALUES (?,?,?,?,?,?,?,?,?,?)";
							$mysqli_stmt->prepare($query);
							$mysqli_stmt->bind_param("ssssiiiiss", $guid, $username, $email, $passhash, $payment, $current_page, $minutes, $hour, $day_of_week, $timezone);
							$mysqli_stmt->execute();
							$mysqli_stmt->free_result();
						}
						/****************************************************************************
							Enter the resulting cronjobs into the crontab.
						****************************************************************************/
						include('/home/thege0/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
// 						include('/Users/iwato/Sites/nudge.online/public_html/spiritof2021.online/cambitas/_utilities/php/includes/page_delivery_incl.php');
						/****************************************************************************
							Reset Page Number in User's Account
						****************************************************************************/
						if (!empty($pageno)) {
							$sql = "UPDATE $tbl_name SET pageno=? WHERE guid=?";
							$mysqli_stmt->prepare($sql);
							$mysqli_stmt->bind_param("is", $pageno, $guid);
							$mysqli_stmt->execute();
							$mysqli_stmt->free_result();
						}
						/****************************************************************************
							Delete the subscribers previous account while retaining his $passhash.
						****************************************************************************/
						$sql = "DELETE FROM $tbl_name WHERE guid=?";
						$mysqli_stmt->prepare($sql);
						$mysqli_stmt->bind_param("s", $old_guid);
						$mysqli_stmt->execute();
						$mysqli_stmt->free_result();
						/****************************************************************************
							Send alert conditioned on the subscriber's action.
						****************************************************************************/
						if (!empty($pageno)) {
							echo "Congratulations, $username! Your schedule has been reset, and your next delivered page will be number $pageno.";						
						} else {
							echo "Congratulations, $username! Your schedule has been reset, and your next delivered page will begin where you left off before making your schedule change.";													
						}
					} else if (($payment == 1) && ($current_page == $page_limit)) { 
						echo "Your free trial period has expired.  If you wish to continue with your subscription, click on the schedule confirmation button for unpaid current subscribers making payment.";				
					}
				} else {
					echo "No password match was found. Please enter the correct password or request a new one from the Spirit of 2021 webmaster at <a href='mailto:admin@spiritof2021.online?Subject=Spirit%20of%202021%20-%20Request%20for%20New%20Password' title='Spirit of 2021 Webmaster' target='_top'>admin@spiritof2021.online</a>.";
				}
			} else {
				echo "No match was found.  Please enter the correct username and email address.";
			}
		}		
	/*************************************************************************************************************************************
		Schedule deactivation and activation with or without restart page option, free chapter access, and notify to make payment
	*************************************************************************************************************************************/
	} else if (!empty($_POST['passphrase']) && !isset($_POST['guid'])) {
		/****************************************************************************
			Filter the subscriber's contact and other info sent by the AJAX routine
		****************************************************************************/
		$passhash = '';
		$payment = 0;
		$page_limit = 2;
		$current_page = 0;
		$pageno = filter_var($_POST['pageno'], FILTER_VALIDATE_INT);
		$newphrase = filter_var($_POST['newphrase'], FILTER_VALIDATE_BOOLEAN);
		$freechap = filter_var($_POST['freechap'], FILTER_VALIDATE_BOOLEAN);
		$username = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$email = filter_var($_POST['email'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_SANITIZE_EMAIL);
		$passphrase = filter_var($_POST['passphrase'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH);
		/****************************************************************************
			Create MySQLi object for query, matching (verification) and upadate
		****************************************************************************/
		include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
		$papa_connect = new PapaConnect();
		$mysqli_obj = $papa_connect->get_mysqli_obj();	
// 		include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 		$iwato_connect = new IwatoConnect();
// 		$mysqli_obj = $iwato_connect->get_mysqli_obj();
		/*****************************************************************************************
			Obtain the subscriber's previous subscription data based on his username and email
		*****************************************************************************************/
		$tbl_name = 'spirit_db.subscribers';
// 		$tbl_name = 'spirit.subscribers';
		$sql_select = "SELECT passhash, payment, pageno, guid FROM $tbl_name WHERE username=? AND email=?";
		$mysqli_stmt = $mysqli_obj->stmt_init();
		$mysqli_stmt->prepare($sql_select);
		$mysqli_stmt->bind_param("ss", $username, $email);
		if ($mysqli_stmt->execute()) {
			$meta = $mysqli_stmt->result_metadata();
			while ($field = $meta->fetch_field()) {
				$params[] = &$row[$field->name];
			}
			call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
			while ($mysqli_stmt->fetch()) {
				foreach($row as $key => $val) {
					$c[$key] = $val;
				}
				$prelim_result[] = $c;
			}
			if ($prelim_result) {
				foreach ($prelim_result as $arr) {
					foreach ($arr as $name => $value){
						if ($name == "passhash") {
							$passhash = $value;
						}
						if ($name == "payment") {
							$payment = $value;
						}
						if ($name == "pageno") {
							$current_page = $value;
						}
						if ($name == "guid") {
							$guid = $value;
						}
					}
				}				
				$mysqli_stmt->free_result();
				if (password_verify($passphrase, $passhash)) {
					/****************************************************************************
						Provide user with link to free copy of First Chapter.
					****************************************************************************/
					if ($freechap == true) {
						echo "Click <a href='./free_download.html' title='Mount Cambitas - The Story of Free Money (Chapter One)' target='_blank'>here</a> to obtain your free copy of Chapter One.  Please allow for a pop-up.";					
					} else {
						/****************************************************************************
							Activation and Deactivation of Scheduled Delivery
						****************************************************************************/
						if ((($payment == 1) && !empty($guid)) || ($payment == 2)) {
							$crontabRepository = new CrontabRepository(new CrontabAdapter());		
							$cronjobs = $crontabRepository->findJobByRegex("/$guid/");
							foreach($cronjobs as $cronjob) {
								foreach ($cronjob as $key => $value) {
									if ($key == 'enabled') {
										if ($value == false) {
											$cronjob->setEnabled(true);
											echo "<p>Your schedule has been reactivated.<br />Delivery will begin again on your next delivery date.</p>";
											/****************************************************************************
												Reset Page Number in User's Account
											****************************************************************************/
											If (!empty($pageno)) {
												$sql = "UPDATE $tbl_name SET pageno=? WHERE guid=?";
												$mysqli_stmt->prepare($sql);
												$mysqli_stmt->bind_param("is", $pageno, $guid);
												$mysqli_stmt->execute();
												$mysqli_stmt->free_result();
												echo "<p>Your new delivery start page is $pageno.</p>";
											}
										} else if ($value == true) {
											$cronjob->setEnabled(false);
											echo "<p>Your schedule has been deactivated and your delivery has temporarily stopped.<br />You may restart delivery by clicking on the same button with which you deactivated your schedule.</p><p>Upon reactivation of your schedule you may also change your current start page (Page $current_page).</p>";
										}
									}
								}
							}
							$crontabRepository->persist();
							$cronjobs = $crontabRepository->findJobByRegex("/$guid/");
						/****************************************************************************
							Notification that subscriber's free trial period has ended.
						****************************************************************************/
						}  else if (($payment == 1) && ($current_page >= $page_limit)) { 
							echo "Your free trial period has expired.  If you wish to continue with your subscription, click on the schedule confirmation button for unpaid current subscribers making payment.";				

						}
					}
				} else {
					echo "No password match was found. Please enter the correct password or request a new one from the Spirit of 2021 webmaster at <a href='mailto:admin@spiritof2021.online?Subject=Spirit%20of%202021%20-%20Request%20for%20New%20Password' title='Spirit of 2021 Webmaster' target='_top'>admin@spiritof2021.online</a>.";
				}
			} else {
				echo "No match was found.  Please enter the correct username and email address.";
			}
		}
	/*******************************************************************************
		Reset the subscriber's passphrase.
	*******************************************************************************/
	} else if (isset($_POST['username']) && isset($_POST['email']) && ($_POST['newphrase'] == true)) {
		$username = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$email = filter_var($_POST['email'], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_SANITIZE_EMAIL);
		$newphrase = filter_var($_POST['newphrase'], FILTER_VALIDATE_BOOLEAN);
		$pageno = filter_var($_POST['pageno'], FILTER_VALIDATE_INT);
		$prelim_result = [];
		if ($newphrase == true) {
			/****************************************************************************
				Connect to database.
			****************************************************************************/
			include_once("/home/thege0/public_html/spiritof2021.online/_utilities/php/classes/class.papaconnect.php");
			$papa_connect = new PapaConnect();
			$mysqli_obj = $papa_connect->get_mysqli_obj();
// 			include_once("../../../_utilities/php/classes/class.iwatoconnect.php");
// 			$iwato_connect = new IwatoConnect();
// 			$mysqli_obj = $iwato_connect->get_mysqli_obj();
			/********************************************************************************
				Initialize crucial variable.
			********************************************************************************/
			$guid = '';
			$pageno = 0;
			$timezone = '';
			$image_link = "<img src='cid:aveverum_banner' max-width='600' width:100% height='145' style='margin:0; padding:0; border:none; display:block;' border='0' alt='The Ave Verum Banner' />";
			/*****************************************************************************************
				Obtain the subscriber's guid based on his username and email address
			*****************************************************************************************/
			$tbl_name = 'spirit_db.subscribers';
// 			$tbl_name = 'spirit.subscribers';
			$sql_select = "SELECT guid, timezone FROM $tbl_name WHERE username=? AND email=?";
			$mysqli_stmt = $mysqli_obj->stmt_init();
			$mysqli_stmt->prepare($sql_select);
			$mysqli_stmt->bind_param("ss", $username, $email);
			/**********************************************************
				Retrieve the user's guid and timezone from Database
			**********************************************************/
			if ($mysqli_stmt->execute()) {
				$meta = $mysqli_stmt->result_metadata();
				while ($field = $meta->fetch_field()) {
					$params[] = &$row[$field->name];
				}
				call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
				while ($mysqli_stmt->fetch()) {
					foreach($row as $key => $val) {
						$c[$key] = $val;
					}
					$prelim_result[] = $c;
				} 
				if (isset($prelim_result)) {
					foreach ($prelim_result as $arr) {
						foreach ($arr as $name => $value){
							$page_results[$name] = $value;
						}
					}
					/****************************************************************************
						Create a confirmation email and send it.
					****************************************************************************/
					$mysqli_stmt->free_result();
					$guid = $page_results['guid'];
					$timezone = $page_results['timezone'];
					/************************************************
						Initiate PHPMailer instance
					************************************************/
					$mail = new PHPMailer(true);
					$mail->CharSet = 'UTF-8';
					$mail->isSMTP();
					$mail->SMTPDebug = 0;
					$mail->Debugoutput = 'html';
					/****************************************************************************
						Connect to spiritof2021.online mailserver.
					****************************************************************************/
					$mail->Host = "nwurq132.hostpapavps.net";
					$mail->Port = 587;
					$mail->Username = "admin@spiritof2021.online";
					$mail->Password = "FCw2UxG7jmeX7L8";
	// 				$mail->Host = "0.0.0.0";
	// 				$mail->Port = 1025;
	// 				$mail->Username = "admin@spiritof2021.online";
	// 				$mail->Password = "nudge.Liberty+";
					/****************************************************************************
						Prepare confirmation email
					****************************************************************************/
					$mail->SMTPAuth = true;
					$mail->setFrom('admin@spiritof2021.online', 'Spirit of 2021');
					$mail->addAddress($email, $username);
					$mail->Subject = 'Account Verification';
					$mail->Priority = 1;
					$mail->addEmbeddedImage('/home/thege0/public_html/spiritof2021.online/cambitas/_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
	// 				$mail->addEmbeddedImage('../../_images/aveverum_banner.png', 'aveverum_banner', 'aveverum_banner.png', 'base64', 'image/png');
					/****************************************************************************
						Prepare message.
					****************************************************************************/
					$html_message = file_get_contents('/home/thege0/public_html/spiritof2021.online/cambitas/new_passphrase.html');
	// 				$html_message = file_get_contents('../../new_passphrase.html');
					$html_message = str_replace('%image%', $image_link, $html_message);
					$html_message = str_replace('%timezone%', $timezone, $html_message);
					$html_message = str_replace('%name%', $username, $html_message);
					$html_message = str_replace('%newphrase%', true, $html_message);
					$html_message = str_replace('%guid%', $guid, $html_message);
					$mail->Body = $html_message;
					$alt_message = "Hi, $username! Your request for a new passphrase has been received. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and enable a new passphrase to be sent to your address. \n\r  https://cambitas.spiritof2021.online/_utilities/php/email_verify.php?newphrase=$newphrase&guid=$guid.";
	// 				$alt_message = "Hi, $username! Your request for a new passphrase has been received. \n\r Please copy the following line of code into your browser window and click enter.  This will verify your email identity and enable a new passphrase to be sent to your address. \n\r  https://cambitas.spiritof2021.local/_utilities/php/email_verify.php?newphrase=$newphrase&guid=$guid.";
					$mail->AltBody = $alt_message;
					/****************************************************************************
						Send email or report failure.
					****************************************************************************/
					if (!$mail->send()) {
						$msg = $mail->ErrorInfo;
					} else {
						echo "<p>Hi, $username! A confirmation email has been sent to the entered address.</p><p>Please click on the indicated link in the email in order to verify that you are, indeed, the author of the requested passphrase.  Once your address has been authenticated, an email containing your new passphrase will be sent.  Depending on internet traffic the usual wait-time is between instantaneous and several minutes.</p>";
					}
				} else {
					echo "No match was found.  Make sure that you have entered the correct username and email address.";
				}
			}
		} else {
			echo "Make sure that you have checked the New Passphrase box.";
		}
	}
?>