<?php
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
/**
Creation Date: 2017/01/12
Author:  Roddy A. Stegemann
Brief:  Creates a class for gathering data from the repository table of the spirit and spirit_db databases for the purpose of filling an email template for the purpose of the delivery of Deflation Made Simple content.
*/
class PageData {
	private $mysqli_obj;
	private $guid;
	private $pageno;
		
	public function __construct($mysqli_obj, $guid, $pageno) {
// 		$params = [];
// 		$prelim_result = [];
// 		$c = [];
// 		$page_results = [];
// 		$row = [];
		$this->mysqli_obj = $mysqli_obj;
		$this->guid = $guid;
		$this->pageno = $pageno;
	}

	public function get_subscriber_data($guid) {
		$params = [];
		$prelim_result = [];
		$c = [];
		$page_results = [];
		$row = [];
		$sql_fetch = "SELECT subscribers.username, subscribers.email, subscribers.timezone, repository.page, repository.chapter, repository.html FROM subscribers LEFT JOIN repository ON subscribers.pageno = repository.page WHERE subscribers.guid = ?";
		$mysqli_stmt = $this->mysqli_obj->stmt_init();
		$mysqli_stmt->prepare($sql_fetch);
		$mysqli_stmt->bind_param("s", $this->guid);
		$mysqli_stmt->execute();
		$meta = $mysqli_stmt->result_metadata();
		while ($field = $meta->fetch_field()) {
			$params[] = &$row[$field->name];
		}
		call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
		while ($mysqli_stmt->fetch()) {
			foreach($row as $key => $val) {
				$c[$key] = $val;
			}
			$prelim_result[] = $c;
		}
		foreach ($prelim_result as $arr) {
			foreach ($arr as $name => $value){
				$page_results[$name] = $value;
			}
		}
		$mysqli_stmt->free_result();
		return $page_results;
	}
	
	public function get_page_data($pageno, $page_format) {
		$tbl_name = "repository";
		$params = [];
		$prelim_result = [];
		$c = [];
		$page_results = [];
		$row = [];
		$sql_fetch = "SELECT page, chapter, $page_format FROM $tbl_name WHERE page=?";
		$mysqli_stmt = $this->mysqli_obj->stmt_init();
		$mysqli_stmt->prepare($sql_fetch);
		$mysqli_stmt->bind_param("i", $pageno);
		$mysqli_stmt->execute();
		$meta = $mysqli_stmt->result_metadata();
		while ($field = $meta->fetch_field()) {
			$params[] = &$row[$field->name];
		}
		call_user_func_array(array($mysqli_stmt, 'bind_result'), $params);
		while ($mysqli_stmt->fetch()) {
			foreach($row as $key => $val) {
				$c[$key] = $val;
			}
			$prelim_result[] = $c;
		}
		foreach ($prelim_result as $arr) {
			foreach ($arr as $name => $value){
				$page_results[$name] = $value;
			}
		}
		$mysqli_stmt->free_result();
		return $page_results;
	}
}
?>