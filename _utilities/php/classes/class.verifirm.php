<?php
	error_reporting(E_ALL);
	ini_set('log_errors', 1);
	ini_set('error_log', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.log');
	ini_set('html_errors', 1);
	ini_set('display_errors', 0);
	class VeriFirm {
		private $mysqli_obj;

		private $username;
		private $email;
		private $hash;
		private $status;
		private $action;

		private $pageno;
		private $tbl_name = 'subscribers';

		public $admin = 'admin@spiritof2021.online';
		public $subject = 'Deflation%20Made%20Simple%20-%20Verify%20and%20Confirm%20User%20Action';
		
		public $msg_mismatch;
		public $msg_success; 
		public $msg_failure;

		public function __construct($mysqli_obj, $action) {
			$this->mysqli_obj = $mysqli_obj;
			if(!empty($_GET['name']) AND !empty($_GET['email']) AND !empty($_GET['guid'])) {
				$this->username = $_GET['name'];
				$this->email = $_GET['email'];
				$this->hash = $_GET['guid'];
				$this->action =  $action;
			}			
		}

		public function create_link() {
			return $mailto = "mailto:" . $this->admin . "?Subject=" . $this->subject;			
		}

		public function update_status() {
			$mysqli_obj = $this->mysqli_obj;
			$mysqli_stmt = $mysqli_obj->stmt_init();
			$sql_select = "SELECT guid username, email" . $this->action . " FROM " . $this->tbl_name . ' WHERE guid=? AND username=? AND email=?';
			$mysqli_stmt->prepare($sql_select);
			$mysqli_stmt->bind_param("iss", $this->guid, $this->username, $this->email);
			$mysqli_stmt->execute();
			$mysqli_result = $mysqli_stmt->get_result();
			$match = mysqli_num_rows($mysqli_result);
			if($match > 0){
				while ($row = $mysqli_result->fetch_assoc()) {
					foreach ($row as $key => $value) {
						$result[$key] = $value;
					}
				}
				$this->status = $result[$this->action];
				if ($this->status == 0) {				
					$sql_update = "UPDATE " . $this->tbl_name . " SET " . $this->field . "='1' WHERE roster_obs=? AND user_name=? AND email_address=?";
					$mysqli_stmt->prepare($sql_update);
					$mysqli_stmt->bind_param("iss", $this->hash, $this->username, $this->email);
					if ($mysqli_stmt->execute()) {
						return $this->msg_success;
					} else {
						return $this->msg_failure;
					}
				} else if ($this->status == 1) {
					$sql_update = "UPDATE " . $this->tbl_name . " SET " . $this->field . "='0' WHERE roster_obs=? AND user_name=? AND email_address=?";
					$mysqli_stmt->prepare($sql_update);
					$mysqli_stmt->bind_param("iss", $this->hash, $this->username, $this->email);
					if ($mysqli_stmt->execute()) {
						return $this->msg_success;
					} else {
						return $this->msg_failure;
					}
				}
			} else {
				return $this->msg_mismatch;
			}
		}
		
		public function get_admin_and_subject() {
			return $this->admin . " and " . $this->subject;			
		}
		public function set_admin_and_subject($admin, $subject) {
			$this->admin = $admin;
			$this->subject = $subject;			
		}

		public function get_tablename() {
			return $this->tbl_name;			
		}
		public function set_tablename($tbl_name) {
			$this->tbl_name = $tbl_name;
		}

		public function get_field() {
			return $this->field;			
		}
		public function set_field($field) {
			$this->field = $field;
		}

		public function get_status() {
			return $this->status;			
		}

		public function get_msg_mismatch() {
			return $this->msg_mismatch;
		}
		public function set_msg_mismatch($msg_mismatch) {
			$this->msg_mismatch = $msg_mismatch;
		}

		public function get_msg_success() {
			return $this->msg_success;
		}
		public function set_msg_success($msg_success) {
			$this->msg_success = $msg_success;
		}

		public function get_msg_failure() {
			return $this->msg_failure;
		}
		public function set_msg_failure($msg_failure) {
			$this->msg_failure = $msg_failure;
		}
	}
?>