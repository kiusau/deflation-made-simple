<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Deflation Made Simple</title>
	<meta name="generator" content="BBEdit 11.6" />
</head>
<body>
<style scoped>
  a:link {color:#000; font-weight: bold; text-decoration: underline;};
  a:hover {color:#ccc; font-weight: bold;}
  a:active {color:#f00; font-weight: bold;}
</style>
	<table style="background-color: #ccc; width:100%; border: 0px; padding: 0px;">
		<tr>
			<td>
				<table style="table-layout: fixed; width: 640px; margin: auto; background-color: #444444; padding: 10px;">
<!-- Begin Header Row -->
					<tr>
						<td>
<!-- Begin top content row including title -->
							<table>
								<tr>
									<td style="background-color:#444444; color: #fff; width: 640px; height: 50px; vertical-align: bottom; font-size: 1.8em; font-family:'Bradley Hand', cursive;">Hi, %name%</td>
								</tr>
							</table>
<!-- End top content row including title -->
						</td>
					</tr>
<!-- End Header Row -->
<!-- Begin Body Row -->
					<tr>
						<td>
							<table style="margin-left: -3px;">
<!-- Begin middle content row -->
								<tr>
<!-- Begin left column content -->
									<td  style="width: 440px; border: 0px; background-color: #aaa; vertical-align: top;">
										<table style="color: #333; width: 100%; padding-left: 2px;">
											<tr>
												<td><span style='font-weight: bold; color: #fff; font-size: 1.2em'>Congratulations!</span><br /><span style="color: #fff;">Your Spirit of 2021 account has been created.</span><br /><br /></td>
											</tr>
											<tr>
												<td style="vertical-align: top;">Click <a href='https://cambitas.spiritof2021.local/_utilities/php/email_verify.php?payment=%payment%&guid=%guid%' title='Confirmation mail for new Deflation Made Simple account and access to free download of Chapter One of Deflation Made Simple' target='_blank'> HERE</a> to											
												</td>
											</tr>
											<tr>
												<td style="vertical-align: top;">1) verify that you are the account owner,<br />2) activate your account, and<br />3) obtain your <span style="font-family:'Bradley Hand', cursive;color: #fff;">free introduction</span> to Deflation Made Simple<br />or begin your subscription now.</td>
											</tr>
											<tr>
												<td style='color: #fff'><br /><span style='font-weight: bold;'>The story of real money<br />is the missing story that reveals so much.</span></td>
											</tr>
										</table>
									</td>
<!-- End left column content -->
<!-- Begin right column content -->
									<td style="width: 200px; background-color: #444444; border: 0px; color: #ffffff; text-align:center; vertical-align: middle;"><span style="font-size:1.6em;">Ave Verum</span></td>
<!-- End right column content -->
								</tr>
<!-- End middle content row -->
							</table>
						</td>
					</tr>
<!-- End Body Row -->
<!-- Begin Footer Row -->
					<tr  style="width: 640px; background-color: #fadb9d;">
						<td>
							<table style="width: 640px; padding: 0px 10px 5px 5px; border: 0px;">
								<tr>
									<td colspan='2' style="padding-top: 10px;">Deflation Made Simple<br />
								</tr>
								<tr>
									<td style='width: 50%; font-size: 0.8em; text-align: left; color: #000;'><span style='text-decoration: none;'>First Hill, Seattle</span><br /><span style='text-decoration: none;'>State of Washington, USA  98014</span></td>
									<td style='width: 50%; font-size: 0.8em; text-align: right; padding: 5px 20px 5px 5px;'><a style='font-weight: 400;text-decoration: none;' href='mailto:admin@grammarcaptive.com?subject=Grammar%20Captive%20Inquiry' title='Grammar Captive Administration'>admin@spiritof2021.online</a><br />telephone:  <span style='text-decoration: none;'>+1 (206) 291-8468</span></td>
								</tr>
							</table>
						</td>
					</tr>
<!-- End Footer Row -->
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
